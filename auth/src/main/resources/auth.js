let thisPage = location.origin + location.pathname; // Path to this file
let failurePage = location.origin + "/authorization-failure.html";

let hashValues = {
    access_token: null,
    scope: ""
};

function getAccessTokenFromHash() {
    extractAccessToken();
    if (hashValues.access_token != null && hashValues.access_token.length > 0) {
        document.location.replace(thisPage + "?access_token=" + hashValues.access_token + "&scope=" + hashValues.scope + "&state=" + hashValues.state);
    } else {
        document.location.replace(failurePage + "?error=access_denied&error_description=no_access_token_found");
    }
}

function extractAccessToken() {
    let hash = document.location.hash;
    let params = hash.slice(1).split('&');
    for (let i = 0; i < params.length; i++) {

        let param = params[i].split('=');
        if (param[0] === "access_token") {
            hashValues.access_token = param[1];
        } else if (param[0] === "scope") {
            hashValues.scope = param[1];
        } else if (param[0] === "state") {
           hashValues.state = param[1];
       }
    }
}

window.onload = () => {
    if (document.location.search.length > 0) {
        window.close();
    } else {
        getAccessTokenFromHash();
    }
};
