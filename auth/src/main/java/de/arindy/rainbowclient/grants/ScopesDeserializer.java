package de.arindy.rainbowclient.grants;

import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JsonDeserializer;
import java.io.IOException;

class ScopesDeserializer extends JsonDeserializer<Scopes> {

    @Override
    public Scopes deserialize(final JsonParser jsonParser, final DeserializationContext deserializationContext) throws IOException {
        return Scopes.fromString(jsonParser.getValueAsString());
    }

}
