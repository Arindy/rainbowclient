package de.arindy.rainbowclient.grants;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import java.util.Collection;

/**
 * When requesting authorization from users, the URL scope parameter allows you to specify
 * which permissions your app requires. These scopes are tied to the access token you
 * receive on successful authorization.
 * Without specifying scopes, your app can access only basic information
 * about the authenticated user.
 * <p>
 * You can specify any or all of these scopes.
 */
@JsonFormat(shape = JsonFormat.Shape.OBJECT)
@JsonDeserialize(using = ScopesDeserializer.class)
public enum Scopes {

    /**
     * Read whether a user is subscribed to your channel.
     */
    CHANNEL_CHECK_SUBSCRIPTION("channel_check_subscription"),

    /**
     * Trigger commercials on channel.
     */
    CHANNEL_COMMERCIAL("channel_commercial"),

    /**
     * Write channel metadata (game, status, etc).
     */
    CHANNEL_EDITOR("channel_editor"),

    /**
     * Add posts and reactions to a channel feed.
     */
    CHANNEL_FEED_EDIT("channel_feed_edit"),

    /**
     * View a channel feed.
     */
    CHANNEL_FEED_READ("channel_feed_read"),

    /**
     * Read nonpublic channel information, including email address and stream value.
     */
    CHANNEL_READ("channel_read"),

    /**
     * Reset a channel’s stream value.
     */
    CHANNEL_STREAM("channel_stream"),

    /**
     * Read all subscribers to your channel.
     */
    CHANNEL_SUBSCRIPTIONS("channel_subscriptions"),

    /**
     * Manage a user’s collections (of videos).
     */
    COLLECTIONS_EDIT("collections_edit"),

    /**
     * Manage a user’s communities.
     */
    COMMUNITIES_EDIT("communities_edit"),

    /**
     * Manage community moderators.
     */
    COMMUNITIES_MODERATE("communities_moderate"),

    /**
     * Turn on/off ignoring a user. Ignoring a user means you cannot see him type, receive messages from him, etc.
     */
    USER_BLOCKS_EDIT("user_blocks_edit"),

    /**
     * Read a user’s list of ignored users.
     */
    USER_BLOCKS_READ("user_blocks_read"),

    /**
     * Manage a user’s followed channels.
     */
    USER_FOLLOWS_EDIT("user_follows_edit"),

    /**
     * Read nonpublic user information, like email address.
     */
    USER_READ("user_read"),

    /**
     * Read a user’s subscriptions.
     */
    USER_SUBSCRIPTIONS("user_subscriptions"),

    /**
     * Turn on Viewer Heartbeat Service ability to record user data.
     */
    VIEWING_ACTIVITY_READ("viewing_activity_read"),

    /**
     * View live stream chat and rooms messages.
     */
    CHAT_READ("chat:read"),

    /**
     * Send live stream chat and rooms messages.
     */
    CHAT_EDIT("chat:edit");

    private final String value;

    Scopes(final String value) {
        this.value = value;
    }

    /**
     * Combine <code>Scopes</code> into a '+' separated <code>String</code>.
     * This is the required input format for twitch.tv
     *
     * @param scopes <code>Scopes</code> to combine.
     * @return <code>String</code> representing '+' separated list of <code>Scopes</code>
     */
    public static String join(final Collection<Scopes> scopes) {
        final String result;
        if (scopes == null) {
            result = "";
        } else {
            final StringBuilder sb = new StringBuilder();
            for (Scopes scope : scopes) {
                if (sb.length() == 0) {
                    sb.append(scope.toString());
                } else {
                    sb.append("+").append(scope.toString());
                }
            }
            result = sb.toString();
        }
        return result;
    }

    /**
     * Convert the string representation of the Scope to the Enum.
     *
     * @param text Text representation of Enum value
     * @return Enum value that the text represents
     */
    public static Scopes fromString(final String text) {
        Scopes result = null;
        if (text != null) {
            for (Scopes b : Scopes.values()) {
                if (text.equalsIgnoreCase(b.value)) {
                    result = b;
                    break;
                }
            }
        }
        return result;
    }

    @Override
    public String toString() {
        return value;
    }
}
