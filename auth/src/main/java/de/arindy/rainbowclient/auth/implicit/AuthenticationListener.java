package de.arindy.rainbowclient.auth.implicit;

import de.arindy.rainbowclient.grants.Scopes;

interface AuthenticationListener {

    void onAccessTokenReceived(String token, Scopes... scopes);

    void onAuthenticationError(String error, String description);

}
