package de.arindy.rainbowclient.auth.implicit;

import com.google.api.client.util.store.DataStore;
import com.google.api.client.util.store.DataStoreFactory;
import de.arindy.rainbowclient.grants.Scopes;
import lombok.Builder;
import lombok.Getter;
import java.io.IOException;
import java.io.Serializable;
import java.util.Arrays;

@Builder
@Getter
public class Credential implements Serializable {

    private static final long serialVersionUID = -8107477954619209650L;

    private final String userName;
    private final String accessToken;
    private final Scopes[] scopes;

    public static DataStore<Credential> getDefaultDataStore(final DataStoreFactory dataStoreFactory) throws IOException {
        return dataStoreFactory.getDataStore(Credential.class.getSimpleName());
    }

    @Override
    public String toString() {
        return "Credential{" +
            "userName='" + userName + '\'' +
            ", accessToken='" + accessToken + '\'' +
            ", scopes=" + Arrays.toString(scopes) +
            '}';
    }

}
