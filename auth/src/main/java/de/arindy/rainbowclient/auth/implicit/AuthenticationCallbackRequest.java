package de.arindy.rainbowclient.auth.implicit;

import de.arindy.rainbowclient.grants.Scopes;
import lombok.extern.slf4j.Slf4j;
import java.io.*;
import java.net.Socket;
import java.net.URL;
import java.net.URLDecoder;
import java.util.HashMap;
import java.util.Map;
import java.util.StringTokenizer;

@Slf4j
class AuthenticationCallbackRequest implements Runnable {

    private static final String EOL = "\r\n";
    private static final int MEGABYTE = 1024;

    private final Socket socket;
    private final URL authPage;
    private final URL failurePage;
    private final URL successPage;
    private final String state;

    private boolean stateMismatch;
    private AuthenticationListener authenticationListener;

    AuthenticationCallbackRequest(final Socket socket, final URL authPage, final URL failurePage, final URL successPage, final String state, final AuthenticationListener authenticationListener) {
        this.socket = socket;
        this.authPage = authPage;
        this.failurePage = failurePage;
        this.successPage = successPage;
        this.state = state;
        this.authenticationListener = authenticationListener;
    }

    private void processRequest() throws IOException {
        final DataOutputStream outputStream = new DataOutputStream(socket.getOutputStream());
        final BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(socket.getInputStream()));

        final String requestLine = bufferedReader.readLine();

        final StringTokenizer tokens = new StringTokenizer(requestLine);
        tokens.nextToken();
        final String requestFilename = tokens.nextToken();
        final Map<String, String> queryParams = extractQueryParams(requestFilename);

        final String accessToken = queryParams.get("access_token");

        final String[] scopes;
        if (queryParams.containsKey("scope")) {
            scopes = queryParams.get("scope").split(" ");
        } else {
            scopes = new String[0];
        }

        final String error = queryParams.get("error");
        final String errorDescription = queryParams.get("error_description");
        final InputStream inputStream;
        final String contentTypeLine;

        if (requestFilename.startsWith("/auth.js")) {
            inputStream = getClass().getResourceAsStream(requestFilename);
            contentTypeLine = "Content-type: text/javascript" + EOL;
        } else {
            if (error != null) {
                inputStream = failurePage.openStream();
            } else if (accessToken != null) {
                if (this.state != null && queryParams.get("state") != null && !queryParams.get("state").equals(this.state)) {
                    this.stateMismatch = true;
                    inputStream = failurePage.openStream();
                } else {
                    inputStream = successPage.openStream();
                }
            } else {
                inputStream = authPage.openStream();
            }
            contentTypeLine = "Content-type: text/html" + EOL;
        }

        final boolean fileExists = inputStream != null;

        final String statusLine;
        final String entityBody;
        if (fileExists) {
            if (stateMismatch) {
                statusLine = "HTTP/1.1 403 Forbidden" + EOL;
                entityBody = "403 state mismatch";
            } else {
                statusLine = "HTTP/1.1 200 OK" + EOL;
                entityBody = "";
            }
        } else {
            statusLine = "HTTP/1.1 404 Not Found" + EOL;
            entityBody = "404 Not Found";
        }

        outputStream.writeBytes(statusLine);
        outputStream.writeBytes(contentTypeLine);
        outputStream.writeBytes(EOL);

        if (fileExists && !stateMismatch) {
            sendFileBytes(inputStream, outputStream);
            inputStream.close();
        } else {
            outputStream.writeBytes(entityBody);
        }

        outputStream.close();
        bufferedReader.close();
        socket.close();

        if (authenticationListener != null) {
            if (accessToken != null && !stateMismatch) {
                final Scopes[] accessScopes = new Scopes[scopes.length];
                for (int i = 0; i < scopes.length; i++) {
                    accessScopes[i] = Scopes.fromString(scopes[i]);
                }
                authenticationListener.onAccessTokenReceived(accessToken, accessScopes);
            }
            if (error != null || stateMismatch) {
                authenticationListener.onAuthenticationError(error, errorDescription);
            }
        }
    }

    private Map<String, String> extractQueryParams(final String request) {
        final Map<String, String> params = new HashMap<>();

        final String[] parts = request.split("\\?", 2);
        if (parts.length >= 2) {
            final String query = parts[1];
            for (String param : query.split("&")) {
                final String[] pair = param.split("=");
                try {
                    params.put(
                        URLDecoder.decode(pair[0], "UTF-8"),
                        pair.length > 1 ? URLDecoder.decode(pair[1], "UTF-8") : ""
                    );
                } catch (UnsupportedEncodingException e) {
                    log.error(e.getMessage(), e);
                }
            }
        }
        return params;
    }

    private static void sendFileBytes(final InputStream inputStream, final OutputStream outputStream) throws IOException {
        final byte[] buffer = new byte[MEGABYTE];
        int bytes;
        while ((bytes = inputStream.read(buffer)) != -1) {
            outputStream.write(buffer, 0, bytes);
        }
    }

    @Override
    public void run() {
        try {
            processRequest();
        } catch (IOException e) {
            log.error(e.getMessage(), e);
        }
    }

}
