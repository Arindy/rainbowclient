package de.arindy.rainbowclient.auth.implicit;

public class AuthenticationError extends Exception {

    AuthenticationError(final String message) {
        super(message);
    }

    AuthenticationError(final String message, final Throwable cause) {
        super(message, cause);
    }

}
