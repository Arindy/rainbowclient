package de.arindy.rainbowclient.auth.implicit;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.api.client.util.Strings;
import com.google.api.client.util.store.DataStore;
import com.google.api.client.util.store.FileDataStoreFactory;
import de.arindy.rainbowclient.grants.Scopes;
import lombok.Builder;
import lombok.NonNull;
import lombok.extern.slf4j.Slf4j;
import org.apache.http.HttpEntity;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.HttpClients;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.net.URI;
import java.net.URISyntaxException;
import java.nio.charset.StandardCharsets;
import java.util.Base64;
import java.util.Collection;
import java.util.Objects;
import static java.awt.Desktop.getDesktop;
import static java.awt.Desktop.isDesktopSupported;

@Slf4j
public class Authenticator {

    private static final int UNAUTHORIZED = 401;
    private static final int MIN_EXPIRES_IN = 60;
    private static final int DEFAULT_PORT = 49825;
    private static final URI DEFAULT_REDIRECT_URI = URI.create("http://127.0.0.1:" + DEFAULT_PORT + "/authorize.html");
    private static final File DATA_STORE_DIR = new File(System.getProperty("user.home"), ".store/rainbowClient");

    private DataStore<Credential> dataStore;

    private final String oauthAuthorizeUrl = "https://id.twitch.tv/oauth2/authorize";
    private final String clientId;
    private final URI redirectUri;
    private final Collection<Scopes> scopes;
    private String state;
    private final boolean forceVerify;
    private AuthenticationCallbackServer server;

    public static boolean validate(final Credential credential) throws AuthenticationError {
        final boolean result;
        if (credential == null) {
            result = false;
        } else {
            result = validateToken(credential);
        }
        return result;
    }

    private static boolean validateToken(final Credential credential) throws AuthenticationError {
        boolean result;
        try {
            final HttpGet request = new HttpGet("https://id.twitch.tv/oauth2/validate");
            request.addHeader("Authorization", String.format("OAuth %s", credential.accessToken()));
            result = validateToken(credential, ((HttpClient) HttpClients.createDefault()).execute(request).getEntity());
        } catch (IOException e) {
            log.error(e.getMessage(), e);
            result = false;
        }
        return result;
    }

    private static boolean validateToken(final Credential credential, final HttpEntity entity) throws AuthenticationError {
        boolean result;
        if (entity == null) {
            result = false;
        } else {
            try (InputStream inputStream = entity.getContent()) {
                final JsonNode jsonNode = new ObjectMapper().readTree(inputStream);
                if (unauthorized(jsonNode) || expiresSoon(jsonNode) || jsonNode.get("login") == null) {
                    result = false;
                } else {
                    if (!Objects.equals(
                        credential.userName(),
                        jsonNode.get("login").toString().replaceAll("\"", "")
                    )) {
                        throw new AuthenticationError("UserNames don't match");
                    } else {
                        result = true;
                    }
                }
            } catch (IOException e) {
                log.error(e.getMessage(), e);
                result = false;
            }
        }
        return result;
    }

    private static boolean expiresSoon(final JsonNode node) {
        return node.get("expires_in") != null && node.get("expires_in").asInt() <= MIN_EXPIRES_IN;
    }

    private static boolean unauthorized(final JsonNode node) {
        return node.get("status") != null && node.get("status").asInt() == UNAUTHORIZED;
    }

    @Builder
    public Authenticator(@NonNull final String clientId, final URI redirectUri, final String state, final boolean forceVerify, final Collection<Scopes> scopes) {
        this.clientId = clientId;
        this.redirectUri = redirectUri;
        this.scopes = scopes;
        this.state = state;
        this.forceVerify = forceVerify;
        try {
            dataStore = Credential.getDefaultDataStore(new FileDataStoreFactory(DATA_STORE_DIR));
        } catch (IOException e) {
            log.warn(e.getMessage(), e);
        }
    }

    public Credential authorize(final String userName) throws AuthenticationError {
        final Credential result;
        final Credential credential = loadCredential(userName);
        if (Authenticator.validate(credential)) {
            result = credential;
        } else {
            result = createAndStoreCredential(userName);
        }
        return result;
    }

    public Credential createAndStoreCredential(final String userName) throws AuthenticationError {
        final Credential credential;
        try {
            if (isDesktopSupported() && getDesktop().isSupported(java.awt.Desktop.Action.BROWSE)) {
                getDesktop().browse(new URI(buildRequest()));
                server.start();
                if (server.hasAuthenticationError()) {
                    log.error(server.getAuthenticationError().toString());
                }
                credential = Credential.builder().userName(userName).accessToken(server.accessToken()).scopes(server.scopes()).build();
            } else {
                throw new AuthenticationError("Can't open TokenRequest-Page!");
            }
            if (dataStore != null) {
                dataStore.set(userName, credential);
            }
        } catch (IOException | URISyntaxException e) {
            throw new AuthenticationError(e.getMessage(), e);
        }
        return credential;
    }

    public Credential loadCredential(final String userName) {
        Credential result;
        if (Strings.isNullOrEmpty(userName)) {
            result = null;
        } else if (dataStore == null) {
            result = null;
        } else {
            try {
                result = dataStore.get(userName);
                if (!validate(result)) {
                    result = null;
                }
            } catch (AuthenticationError | IOException e) {
                log.error(e.getMessage(), e);
                result = null;
            }
        }
        return result;
    }

    private String buildRequest() {
        if (redirectUri == null) {
            this.server = AuthenticationCallbackServer.builder().port(DEFAULT_PORT).state(generateState()).build();
        }
        final String request = String.format("%s?client_id=%s&redirect_uri=%s&response_type=token%s%s%s",
            oauthAuthorizeUrl,
            this.clientId,
            this.redirectUri == null ? DEFAULT_REDIRECT_URI : this.redirectUri,
            this.forceVerify ? "&force_verify=true" : "",
            this.scopes != null ? String.format("&scope=%s", Scopes.join(scopes)) : "",
            this.state != null ? String.format("&state=%s", this.state) : ""
        );
        log.trace(this.getClass().toString() + ": " + request);
        return request;
    }

    private String generateState() {
        if (this.redirectUri == null && this.state == null) {
            this.state = Base64.getEncoder().encodeToString((this.clientId + "@" + DEFAULT_REDIRECT_URI).getBytes(StandardCharsets.UTF_8));
        }
        return this.state;
    }

}
