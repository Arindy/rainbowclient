package de.arindy.rainbowclient.auth.implicit;

import de.arindy.rainbowclient.grants.Scopes;
import lombok.Builder;
import lombok.Getter;
import lombok.extern.slf4j.Slf4j;
import java.io.IOException;
import java.net.InetAddress;
import java.net.ServerSocket;
import java.net.SocketException;
import java.util.Arrays;

@Slf4j
public class AuthenticationCallbackServer implements AuthenticationListener {

    private static final String DEFAULT_AUTH_PAGE = "/authorize.html";
    private static final String DEFAULT_FAILURE_PAGE = "/authorize-failure.html";
    private static final String DEFAULT_SUCCESS_PAGE = "/authorize-success.html";

    private ServerSocket serverSocket;
    private AuthenticationError authenticationError;
    private String state;
    @Builder.Default
    private int port = 80;

    @Getter
    private String accessToken;
    @Getter
    private Scopes[] scopes;

    @Builder
    public AuthenticationCallbackServer(final String state, final int port) {
        this.state = state;
        this.port = port;
    }

    void start() throws IOException {
        log.debug("Starting Server...");
        serverSocket = new ServerSocket(this.port, 0, InetAddress.getByName("127.0.0.1"));
        run();
    }

    private void run() throws IOException {
        while (true) {
            try {
                new Thread(
                    new AuthenticationCallbackRequest(
                        serverSocket.accept(),
                        getClass().getResource(DEFAULT_AUTH_PAGE),
                        getClass().getResource(DEFAULT_FAILURE_PAGE),
                        getClass().getResource(DEFAULT_SUCCESS_PAGE),
                        state,
                        this),
                    "AuthenticationCallbackRequest"
                ).start();
            } catch (SocketException e) {
                break;
            }
        }
    }

    @Override
    public void onAccessTokenReceived(final String token, final Scopes... scopes) {
        log.trace(String.format("AccessToken Recieved: scopes[%s]", Arrays.toString(scopes)));
        this.accessToken = token;
        this.scopes = scopes;
        stop();
    }

    @Override
    public void onAuthenticationError(final String error, final String description) {
        final String message = String.format("Authentication Error: %s: %s", error, description);
        log.warn(message);
        authenticationError = new AuthenticationError(message);
        stop();
    }

    private void stop() {
        log.debug("Stopping Server...");
        if (serverSocket != null && !serverSocket.isClosed()) {
            try {
                serverSocket.close();
            } catch (IOException e) {
                log.debug(e.getMessage(), e);
            } finally {
                serverSocket = null;
            }
        }
    }

    AuthenticationError getAuthenticationError() {
        return authenticationError;
    }

    boolean hasAuthenticationError() {
        return authenticationError != null;
    }

}
