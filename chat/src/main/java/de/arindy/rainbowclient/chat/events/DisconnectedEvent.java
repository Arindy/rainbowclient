package de.arindy.rainbowclient.chat.events;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.ToString;

@Getter
@ToString
@AllArgsConstructor
public class DisconnectedEvent implements ChatEvent {

    private final int code;
    private final String reason;

}
