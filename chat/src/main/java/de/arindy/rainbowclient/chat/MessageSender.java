package de.arindy.rainbowclient.chat;

public interface MessageSender {

    void sendMessage(String message) throws NotConnectedException;

}
