package de.arindy.rainbowclient.chat;

import de.arindy.rainbowclient.auth.implicit.AuthenticationError;
import de.arindy.rainbowclient.chat.events.EventListener;
import lombok.Builder;
import lombok.NonNull;
import lombok.Singular;
import lombok.extern.slf4j.Slf4j;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map;
import java.util.regex.Pattern;

@Slf4j
public final class Chat implements AutoCloseable {

    public static final int QUEUE_SIZE = 200;
    public static final int INITIAL_CAPACITY = 20;
    public static final int INITIAL_SECONDS = 30;

    private final ChatConnecter chatConnecter;
    private final Map<String, Channel> channels;
    private final MessageWorker messageWorker;

    @Builder(buildMethodName = "connect")
    private Chat(@NonNull final String webSocketServer, @NonNull final String clientId, @NonNull final String userName, @Singular(ignoreNullCollections = true) final Collection<EventListener> eventListeners) throws AuthenticationError {
        chatConnecter = new ChatConnecter(webSocketServer, clientId, eventListeners).authenticate(userName).connect();
        channels = new HashMap<>();
        this.messageWorker = new MessageWorker(
            QUEUE_SIZE, INITIAL_CAPACITY, INITIAL_SECONDS,
            chatConnecter::send
        );
        new Thread(messageWorker, "MessageWorker").start();
    }

    public Channel joinChannel(final String channel) throws NotConnectedException {
        log.info(String.format("Joining Channel [%s]...", channel.toLowerCase()));
        final Channel result;
        if (channels.containsKey(channel)) {
            result = channels.get(channel);
        } else {
            messageWorker.send("join #%s", channel.toLowerCase());
            result = new Channel(channel.toLowerCase(), this::sendMessage);
            channels.put(channel, result);
        }
        return result;
    }

    public void leaveChannel(final Channel channel) throws NotConnectedException {
        leaveChannel(channel.name());
    }

    public void leaveChannel(final String channel) throws NotConnectedException {
        if (channels.containsKey(channel)) {
            log.info(String.format("Leaving Channel [%s]...", channel.toLowerCase()));
            messageWorker.send("part #%s", channel.toLowerCase());
            channels.remove(channel);
        }
    }

    public void sendWhisper(final String user, final String message) throws NotConnectedException {
        log.info(String.format("Sending Whisper [%s]: %s", chatConnecter.credential().userName(), message));
        messageWorker.send("PRIVMSG #%s :/w %s %s", chatConnecter.credential().userName(), user, message);
    }

    public void changeColor(final Color color) throws NotConnectedException {
        changeColor(color.toString());
    }

    public void changeColor(final String color) throws NotConnectedException {
        if (Color.exists(color) || Pattern.matches("#[A-Fa-f\\d]{6}", color)) {
            log.info(String.format("Changing Color to %s", color));
            messageWorker.send("PRIVMSG #%s :/color %s", chatConnecter.credential().userName(), color);
        }
    }

    private void sendMessage(final String message) throws NotConnectedException {
        messageWorker.send(message);
    }

    public void disconnect() {
        disconnectFromAllChannels();
        messageWorker.stop();
        chatConnecter.disconnect();
    }

    public void register(final EventListener eventListener) {
        chatConnecter.register(eventListener);
    }

    public boolean connected() {
        return chatConnecter.connected();
    }

    @Override
    public void close() throws InterruptedException {
        disconnectFromAllChannels();
        chatConnecter.close();
    }

    public void disconnectFromAllChannels() {
        new HashMap<>(channels).forEach((name, channel) -> {
            try {
                leaveChannel(name);
            } catch (NotConnectedException e) {
                log.warn(e.getMessage(), e);
            }
        });
        channels.clear();
    }

}

