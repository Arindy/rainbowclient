package de.arindy.rainbowclient.chat.events;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.ToString;
import java.util.Map;

@Getter
@ToString
@AllArgsConstructor
public class RoomStateEvent implements ChatEvent {

    @Getter
    public enum TAG {

        /**
         * Emote-only mode.
         * If enabled, only emotes are allowed in chat. Valid values: {@code 0} (disabled) or {@code 1} (enabled).
         */
        EMOTE_ONLY("emote-only"),
        /**
         * Followers-only mode.
         * If enabled, controls which followers can chat.
         * Valid values: {@code -1} (disabled), {@code 0} (all followers can chat), or a non-negative integer (only users following for at least the specified number of minutes can chat).
         */
        FOLLOWERS_ONLY("followers-only"),
        /**
         * R9K mode.
         * If enabled, messages with more than 9 characters must be unique.
         * Valid values: {@code 0} (disabled) or {@code 1} (enabled).
         */
        R9K("r9k"),
        RITUALS("rituals"),
        ROOM_ID("room-id"),
        /**
         * The number of seconds a chatter without moderator privileges must wait between sending messages.
         */
        SLOW("slow"),
        /**
         * Subscribers-only mode.
         * If enabled, only subscribers and moderators can chat.
         * Valid values: {@code } (disabled) or {@code } (enabled).
         */
        SUBS_ONLY("subs-only");

        private String key;

        TAG(final String key) {
            this.key = key;
        }
    }

    private Map<String, String> tags;
    private String channel;

    public boolean hasTag(final TAG tag) {
        return tags.containsKey(tag.key);
    }

    public String tag(final TAG tag) {
        return tags.get(tag.key);
    }
}
