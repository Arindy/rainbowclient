package de.arindy.rainbowclient.chat;

public enum ConnectionState {

    CONNECTING,
    CONNECTED,
    RECONNECTING,
    DISCONNECTING,
    DISCONNECTED

}
