package de.arindy.rainbowclient.chat.events;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.ToString;
import java.util.Map;

@Getter
@ToString
@AllArgsConstructor
public class UserNoticeEvent implements ChatEvent {

    public enum TAG {
        /**
         * Metadata related to the chat badges in the badges tag.
         * <p/>
         * Currently this is used only for subscriber, to indicate the exact number of months the user has been a subscriber.
         * This number is finer grained than the version number in badges.
         * For example, a user who has been a subscriber for 45 months would have a badge-info value of 45 but might have a badges version number for only 3 years.
         */
        BADGE_INFO("badge-info"),
        /**
         * Comma-separated list of chat badges and the version of each badge (each in the format <badge>/<version>, such as admin/1).
         * There are many valid badge values;
         * e.g., admin, bits, broadcaster, global_mod, moderator, subscriber, staff, turbo.
         * Many badges have only 1 version, but some badges have different versions (images), depending on how long you hold the badge status; e.g., subscriber.
         */
        BADGES("badges"),
        /**
         * Hexadecimal RGB color code; the empty string if it is never set.
         */
        COLOR("color"),
        /**
         * The user’s display name, escaped as described in the
         * <a href="https://ircv3.net/specs/core/message-tags-3.2.html">IRCv3 spec</a>.
         * This is empty if it is never set.
         */
        DISPLAY_NAME("display-name"),
        /**
         * Information to replace text in the message with emote images.
         * This can be empty. Syntax:
         * {@code <emote ID>:<first index>-<last index>,<another first index>-<another last index>/<another emote ID>:<first index>-<last index>...}
         * <ul>
         * <li>{@code emote ID} – The number to use in this URL:
         * {@code http://static-cdn.jtvnw.net/emoticons/v1/:<emote ID>/:<size>}
         * ({@code size} is {@code 1.0}, {@code 2.0} or {@code 3.0}.)</li>
         * <li>{@code first index}, {@code last index} – Character indexes. {@code \001ACTION} does not count. Indexing starts from the first character that is part of the user’s actual message. See the example (normal message) below:
         * <p/>
         * The first Kappa (emote ID 25) is from character 0 (K) to character 4 (a), and the other Kappa is from 12 to 16.
         * <p/>
         *     {@code emotes=25:0-4,12-16/1902:6-10}
         * </li>
         * </ul>
         */
        EMOTES("emotes"),
        /**
         * A unique ID for the message.
         */
        ID("id"),
        /**
         * The name of the user who sent the notice.
         */
        LOGIN("login"),
        /**
         * The message. This is omitted if the user did not enter a message.
         */
        MESSAGE("message"),
        /**
         * 1 if the user has a moderator badge; otherwise, 0.
         */
        MOD("mod"),
        /**
         * The <i>type</i> of notice (not the ID).
         * Valid values: {@code sub}, {@code resub}, {@code subgift}, {@code anonsubgift}, {@code submysterygift}, {@code giftpaidupgrade}, {@code rewardgift}, {@code anongiftpaidupgrade}, {@code raid}, {@code unraid}, {@code ritual}, {@code bitsbadgetier}.
         */
        MSG_ID("msg-id"),
        /**
         * The channel ID.
         */
        ROOM_ID("room-id"),
        /**
         * The message printed in chat along with this notice.
         */
        SYSTEM_MSG("system-msg"),
        /**
         * Timestamp when the server received the message.
         */
        TMI_SENT_TS("tmi-sent-ts"),
        /**
         * The user’s ID.
         */
        USER_ID("user-id"),
        /**
         * (Sent only on {@code sub}, {@code resub}) The total number of months the user has subscribed.
         * This is the same as {@code msg-param-months} but sent for different types of user notices.
         */
        MSG_PARAM_CUMULATIVE_MONTHS("msg-param-cumulative-months");
        // TODO: 14/04/2020 https://dev.twitch.tv/docs/irc/tags#clearchat-twitch-tags

        private String key;

        TAG(final String key) {
            this.key = key;
        }
    }

    private final Map<String, String> tags;
    private final String channel;
    private final String message;

    public boolean hasTag(final TAG tag) {
        return tags.containsKey(tag.key);
    }

    public String tag(final TAG tag) {
        return tags.get(tag.key);
    }

}
