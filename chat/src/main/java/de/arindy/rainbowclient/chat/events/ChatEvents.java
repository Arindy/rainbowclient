package de.arindy.rainbowclient.chat.events;

import de.arindy.rainbowclient.chat.Message;
import lombok.extern.slf4j.Slf4j;

@Slf4j
public final class ChatEvents {

    private ChatEvents() {
    }

    public static ChatEvent create(final Message message) {
        final ChatEvent result;
        switch (message.command()) {
            case "001":
            case "002":
            case "003":
            case "004":
            case "375":
            case "372":
            case "376":
                result = new WelcomeEvent(message.message());
                break;
            case "353":
                result = new NamesEvent(message.channel().replaceAll(".*#", ""), message.message());
                break;
            case "366":
                result = new EndOfNamesEvent(message.channel().replaceAll(".*#", ""), message.message());
                break;
            case "421":
                result = new UnkownCommandEvent(message.user(), message.tags(), message.payload());
                break;
            case "CLEARCHAT":
                result = new ClearChatEvent(message.tags(), message.channel(), message.payload());
                break;
            case "CLEARMSG":
                result = new ClearMessageEvent(message.tags(), message.channel(), message.message());
                break;
            case "GLOBALUSERSTATE":
                result = new GlobalUserStateEvent(message.tags());
                break;
            case "HOSTTARGET":
                result = new HostTargetEvent(message.channel(), message.payload());
                break;
            case "JOIN":
                result = new JoinEvent(message.channel(), message.user());
                break;
            case "MODE":
                result = new ModeEvent(message.channel(), message.payload());
                break;
            case "NOTICE":
                result = new NoticeEvent(message.tags(), message.channel(), message.message());
                break;
            case "PART":
                result = new PartEvent(message.channel(), message.user());
                break;
            case "PRIVMSG":
                result = new MessageEvent(message.user(), message.tags(), message.channel(), message.message());
                break;
            case "RECONNECT":
                result = new ReconnectEvent();
                break;
            case "ROOMSTATE":
                result = new RoomStateEvent(message.tags(), message.channel());
                break;
            case "USERNOTICE":
                result = new UserNoticeEvent(message.tags(), message.channel(), message.message());
                break;
            case "USERSTATE":
                result = new UserStateEvent(message.tags(), message.channel());
                break;
            case "WHISPER":
                result = new WhisperEvent(message.user(), message.tags(), message.channel(), message.message());
                break;
            default:
                result = new UnknownEvent(message.rawMessage(), message.command(), message.user(), message.tags(), message.channel(), message.message());
        }
        return result;
    }

}
