package de.arindy.rainbowclient.chat.events;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.ToString;
import java.util.Map;

@Getter
@ToString
@AllArgsConstructor
public class ClearMessageEvent implements ChatEvent {

    public enum TAG {
        /**
         * Name of the user who sent the message.
         */
        LOGIN("login"),
        /**
         * The message.
         */
        MESSAGE("message"),
        /**
         * UUID of the message.
         */
        TARGET_MESSAGE_ID("target-msg-id");

        private String key;

        TAG(final String key) {
            this.key = key;
        }
    }

    private final Map<String, String> tags;
    private final String channel;
    private final String message;

    public boolean hasTag(final TAG tag) {
        return tags.containsKey(tag.key);
    }

    public String tag(final TAG tag) {
        return tags.get(tag.key);
    }

}
