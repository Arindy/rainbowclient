package de.arindy.rainbowclient.chat.events;

import lombok.Getter;
import java.util.Objects;
import java.util.UUID;

@Getter
public abstract class EventListener {

    private final UUID id;

    protected EventListener() {
        id = UUID.randomUUID();
    }

    public abstract void onEvent(ChatEvent event);

    @Override
    public boolean equals(final Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        final EventListener that = (EventListener) o;
        return id.equals(that.id);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id);
    }

}
