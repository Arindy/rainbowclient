package de.arindy.rainbowclient.chat;

import de.arindy.rainbowclient.rest.ChatEndpoint;
import lombok.Getter;
import java.time.Duration;
import java.util.HashMap;
import java.util.Map;
import static de.arindy.rainbowclient.chat.events.RoomStateEvent.TAG.ROOM_ID;

public class Channel {

    @Getter
    private final String name;
    private final MessageSender messageSender;

    private String id;
    private Map<String, Badge> badges;

    private static String message(final String channel, final String message) {
        return String.format("PRIVMSG #%s :%s", channel, message);
    }

    public Channel(final String name, final MessageSender messageSender) {
        this.name = name;
        this.messageSender = messageSender;
        this.badges = new HashMap<>();
    }

    public void timeout(final String user, final Duration duration, final String reason) throws NotConnectedException {
        sendMessage(String.format("/timeout %s %s%s", user, duration.getSeconds(), reason == null ? "" : String.format(" %s", reason)));
    }

    public void ban(final String user, final String reason) throws NotConnectedException {
        sendMessage(String.format("/ban %s%s", user, reason == null ? "" : String.format(" %s", reason)));
    }

    public void unban(final String user) throws NotConnectedException {
        sendMessage(String.format("/unban %s", user));
    }

    public void sendMessage(final String message) throws NotConnectedException {
        try {
            messageSender.sendMessage(message(name, message));
        } catch (NotConnectedException e) {
            throw new NotConnectedException(String.format("Not connected to channel [%s]", name), e);
        }
    }

    public void updateTags(final Map<String, String> tags) {
        this.id = tags.get(ROOM_ID.key());
        this.badges.clear();
        final Map<String, Map<String, Map<String, Map<String, String>>>> badges = ChatEndpoint.getBadges(id);
        badges.keySet().forEach(
            badgeName -> badges.get(badgeName).get("versions").keySet().forEach(
                version -> this.badges.put(badgeKey(badgeName, version), new Badge(badgeKey(badgeName, version), badges.get(badgeName).get("versions").get(version)))
            )
        );
    }

    public Badge badge(final String name, final String version) {
        return badges.get(badgeKey(name, version));
    }

    private String badgeKey(final String name, final String version) {
        return String.format("%s/%s", name, version);
    }

}
