package de.arindy.rainbowclient.chat.events;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.ToString;
import java.util.Map;

@Getter
@ToString
@AllArgsConstructor
public class MessageEvent implements ChatEvent {

    public enum TAG {
        /**
         * Metadata related to the chat badges in the badges tag.
         * <p/>
         * Currently this is used only for subscriber, to indicate the exact number of months the user has been a subscriber.
         * This number is finer grained than the version number in badges.
         * For example, a user who has been a subscriber for 45 months would have a badge-info value of 45 but might have a badges version number for only 3 years.
         */
        BADGE_INFO("badge-info"),
        /**
         * Comma-separated list of chat badges and the version of each badge (each in the format <badge>/<version>, such as admin/1).
         * There are many valid badge values;
         * e.g., admin, bits, broadcaster, global_mod, moderator, subscriber, staff, turbo.
         * Many badges have only 1 version, but some badges have different versions (images), depending on how long you hold the badge status; e.g., subscriber.
         */
        BADGES("badges"),
        /**
         * (Sent only for Bits messages) The amount of cheer/Bits employed by the user.
         * All instances of these regular expressions:
         * {@code /(^\|\s)<emote-name>\d+(\s\|$)/}
         * (where {@code <emote-name>} is an emote name returned by the <a href="https://dev.twitch.tv/docs/v5/reference/bits/#get-cheermotes">Get Cheermotes</a> endpoint), should be replaced with the appropriate emote:
         * <p/>
         * {@code static-cdn.jtvnw.net/bits/<theme>/<type>/<color>/<size>}
         * <ul>
         * <li>theme – light or dark</li>
         * <li>type – animated or static</li>
         * <li>{@code color – red} for 10000+ Bits, {@code blue} for 5000-9999, {@code green} for 1000-4999, {@code purple} for 100-999, {@code gray} for 1-99</li>
         * size – A digit between 1 and 4
         * </ul>
         */
        BITS("bits"),
        /**
         * Hexadecimal RGB color code; the empty string if it is never set.
         */
        COLOR("color"),
        /**
         * The user’s display name, escaped as described in the
         * <a href="https://ircv3.net/specs/core/message-tags-3.2.html">IRCv3 spec</a>.
         * This is empty if it is never set.
         */
        DISPLAY_NAME("display-name"),
        /**
         * Information to replace text in the message with emote images.
         * This can be empty. Syntax:
         * {@code <emote ID>:<first index>-<last index>,<another first index>-<another last index>/<another emote ID>:<first index>-<last index>...}
         * <ul>
         * <li>{@code emote ID} – The number to use in this URL:
         * {@code http://static-cdn.jtvnw.net/emoticons/v1/:<emote ID>/:<size>}
         * ({@code size} is {@code 1.0}, {@code 2.0} or {@code 3.0}.)</li>
         * <li>{@code first index}, {@code last index} – Character indexes. {@code \001ACTION} does not count. Indexing starts from the first character that is part of the user’s actual message. See the example (normal message) below:
         * <p/>
         * The first Kappa (emote ID 25) is from character 0 (K) to character 4 (a), and the other Kappa is from 12 to 16.
         * <p/>
         *     {@code emotes=25:0-4,12-16/1902:6-10}
         * </li>
         * </ul>
         */
        EMOTES("emotes"),
        /**
         * A unique ID for the message.
         */
        ID("id"),
        /**
         * The message.
         */
        MESSAGE("message"),
        /**
         * 1 if the user has a moderator badge; otherwise, 0.
         */
        MOD("mod"),
        /**
         * The channel ID.
         */
        ROOM_ID("room-id"),
        /**
         * Timestamp when the server received the message.
         */
        TMI_SENT_TS("tmi-sent-ts"),
        /**
         * The user’s ID.
         */
        USER_ID("user-id");

        private String key;

        TAG(final String key) {
            this.key = key;
        }
    }

    private final String user;
    private final Map<String, String> tags;
    private final String channel;
    private final String message;

    public boolean hasTag(final TAG tag) {
        return tags.containsKey(tag.key);
    }

    public String tag(final TAG tag) {
        return tags.get(tag.key);
    }

}
