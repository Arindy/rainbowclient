package de.arindy.rainbowclient.chat.events;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.ToString;
import java.util.Map;

@Getter
@ToString
@AllArgsConstructor
public class UnknownEvent implements ChatEvent {

    private final String rawMessage;
    private final String command;
    private final String user;
    private final Map<String, String> tags;
    private final String channel;
    private final String message;

}
