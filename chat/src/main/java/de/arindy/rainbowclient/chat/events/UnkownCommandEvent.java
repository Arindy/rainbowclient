package de.arindy.rainbowclient.chat.events;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.ToString;
import java.util.Map;

@Getter
@ToString
@AllArgsConstructor
public class UnkownCommandEvent implements ChatEvent {

    private String user;
    private Map<String, String> tags;
    private String payload;

}
