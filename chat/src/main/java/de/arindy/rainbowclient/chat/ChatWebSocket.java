package de.arindy.rainbowclient.chat;

import de.arindy.rainbowclient.auth.implicit.Credential;
import de.arindy.rainbowclient.chat.events.ChatEvent;
import de.arindy.rainbowclient.chat.events.ChatEvents;
import de.arindy.rainbowclient.chat.events.ConnectedEvent;
import de.arindy.rainbowclient.chat.events.DisconnectedEvent;
import de.arindy.rainbowclient.chat.events.EventListener;
import lombok.extern.slf4j.Slf4j;
import org.java_websocket.client.WebSocketClient;
import org.java_websocket.exceptions.WebsocketNotConnectedException;
import org.java_websocket.handshake.ServerHandshake;
import java.net.URI;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.HashSet;
import java.util.Objects;

@Slf4j
public class ChatWebSocket extends WebSocketClient {

    private final Credential credential;

    private Collection<EventListener> eventListeners;

    public ChatWebSocket(final String webSocketServer, final Credential credential) {
        super(URI.create(webSocketServer));
        this.credential = Objects.requireNonNull(credential, "Credentials must not be null!");
        eventListeners = new HashSet<>();
    }

    public ChatWebSocket clearListener() {
        this.eventListeners.clear();
        return this;
    }

    void unRegisterListener(final EventListener eventListener) {
        unRegisterListener(Collections.singleton(eventListener));
    }

    void unRegisterListener(final Collection<EventListener> eventListeners) {
        this.eventListeners.removeAll(eventListeners);
    }

    ChatWebSocket registerListener(final EventListener eventListener) {
        return registerListener(Collections.singleton(eventListener));
    }

    ChatWebSocket registerListener(final Collection<EventListener> eventListeners) {
        this.eventListeners.addAll(eventListeners);
        return this;
    }

    @Override
    public void onOpen(final ServerHandshake serverHandshake) {
        try {
            registerCapabilites();
            signIn();
            triggerEvent(new ConnectedEvent());
        } catch (NotConnectedException e) {
            log.error(e.getMessage(), e);
        }
    }

    @Override
    public void onMessage(final String rawMessage) {
        log.trace(String.format("<<< %s", rawMessage));
        Arrays.asList(rawMessage.replace("\n\r", "\n")
            .replace("\r\n", "\n")
            .replace("\r", "\n").split("\n"))
            .forEach(message -> {
                if (!message.isEmpty()) {
                    if (":tmi.twitch.tv NOTICE * :Login authentication failed".equals(message)) {
                        log.error("Invalid IRC Credentials. Login failed!");
                    } else if (message.contains(":req Invalid CAP command")) {
                        log.error("Failed to acquire requested IRC capabilities!");
                    } else if (message.contains(":tmi.twitch.tv CAP * ACK :")) {
                        Arrays.asList(
                            message.replace(":tmi.twitch.tv CAP * ACK :", "").split(" ")
                        ).forEach(cap -> log.debug("Acquired chat capability: " + cap));
                    } else if (message.contains("PING :tmi.twitch.tv")) {
                        send("PONG :tmi.twitch.tv");
                        log.trace(">>> PONG :tmi.twitch.tv");
                    } else {
                        triggerEvent(ChatEvents.create(new Message(message)));
                    }
                }
            });
    }

    @Override
    public void onClose(final int code, final String reason, final boolean remote) {
        log.trace(String.format("onClose: %d: %s", code, reason));
        triggerEvent(new DisconnectedEvent(code, reason));
    }

    @Override
    public void onError(final Exception exception) {
        log.error(exception.getMessage(), exception);
    }

    private void registerCapabilites() throws NotConnectedException {
        log.info("Registering Capabilities...");
        sendText("CAP REQ :twitch.tv/tags twitch.tv/commands twitch.tv/membership");
        sendText("CAP END");
    }

    private void signIn() throws NotConnectedException {
        log.info(String.format("Signing in... [%s]", credential.userName()));
        sendText("pass oauth:%s", credential.accessToken());
        sendText("nick %s", credential.userName());
    }

    private void sendText(final String message, final Object... args) throws NotConnectedException {
        sendText(String.format(message, args));
    }

    void sendText(final String message) throws NotConnectedException {
        log.trace(String.format(">>> %s", message));
        try {
            send(message);
        } catch (WebsocketNotConnectedException e) {
            throw new NotConnectedException(e.getMessage(), e);
        }
    }

    private void triggerEvent(final ChatEvent event) {
        eventListeners.forEach(eventListener -> eventListener.onEvent(event));
    }
}
