package de.arindy.rainbowclient.chat;

import java.util.Objects;

public enum Color {

    RED("Red"),
    FIREBRICK("Firebrick"),
    ORANGE_RED("OrangeRed"),
    CHOCOLATE("Chocolate"),
    CORAL("Coral"),
    GOLDEN_ROD("GoldenRod"),
    YELLOW_GREEN("YellowGreen"),
    SPRING_GREEN("SpringGreen"),
    GREEN("Green"),
    SEA_GREEN("SeaGreen"),
    CADET_BLUE("CadetBlue"),
    DODGER_BLUE("DodgerBlue"),
    BLUE("Blue"),
    BLUE_VIOLET("BlueViolet");

    private String value;

    Color(final String value) {
        this.value = value;
    }

    public static boolean exists(final String value) {
        boolean result = false;
        for (Color color : values()) {
            result = result || Objects.equals(color.value, value);
        }
        return result;
    }

    @Override
    public String toString() {
        return value;
    }

}
