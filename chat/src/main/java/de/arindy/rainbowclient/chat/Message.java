package de.arindy.rainbowclient.chat;

import lombok.Getter;
import lombok.ToString;
import java.util.Arrays;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

@Getter
@ToString
public class Message {

    public static final String MESSAGE_REGEX = "^(?:@(?<tags>.+?) )?(?<clientName>.+?)(?: (?<command>[A-Z0-9]+))(?: #?(?<channel>.*?)? ?)?(?<payload>[:\\-+](?<message>.+))?$";
    public static final String CLIENT_NAME_REGEX = "^:(.*?)!(.*?)@(.*?).tmi.twitch.tv$";
    public static final Collection<String> CHAT_CLIENT_NAMES = Arrays.asList(
        ":tmi.twitch.tv",
        ":jtv"
    );

    private final String rawMessage;
    private final String command;

    private Map<String, String> tags;
    private String channel;
    private String user;
    private String payload;
    private String message;

    public Message(final String message) {
        rawMessage = message;
        final Matcher matcher = Pattern.compile(MESSAGE_REGEX).matcher(message);
        if (matcher.matches()) {
            this.command = matcher.group("command");
            this.channel = matcher.group("channel");
            this.payload = matcher.group("payload");
            this.message = matcher.group("message");
            this.tags = parseTags(matcher.group("tags"));
            this.user = parseUser(matcher.group("clientName"));
        } else {
            this.command = "NONE";
        }
    }

    private Map<String, String> parseTags(final String tags) {
        final Map<String, String> result = new HashMap<>();
        if (tags != null) {
            for (String tag : tags.trim().split(";")) {
                final String[] tagPair = tag.split("=");
                result.put(tagPair[0], (tagPair.length > 1 && tagPair[1].length() > 0) ? tagPair[1] : null);
            }
        }
        return result;
    }

    private String parseUser(final String clientName) {
        final String result;
        if (CHAT_CLIENT_NAMES.contains(clientName) || clientName == null) {
            result = "<chat>";
        } else {
            final Matcher matcher = Pattern.compile(CLIENT_NAME_REGEX).matcher(clientName);
            if (matcher.matches()) {
                result = matcher.group(1);
            } else {
                result = clientName;
            }
        }
        return result;
    }

}
