package de.arindy.rainbowclient.chat.events;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.ToString;

@Getter
@ToString
@AllArgsConstructor
public class EndOfNamesEvent implements ChatEvent {

    private final String channel;
    private final String message;

}
