package de.arindy.rainbowclient.chat.events;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.ToString;
import java.util.Map;

@Getter
@ToString
@AllArgsConstructor
public class WhisperEvent implements ChatEvent {

    //tags={color=#FF0000, display-name=innere_gaenseblume, turbo=0, user-id=149641820, message-id=14, thread-id=39165563_149641820}

    private final String user;
    private final Map<String, String> tags;
    private final String channel;
    private final String message;

}
