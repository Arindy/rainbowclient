package de.arindy.rainbowclient.chat;

import lombok.Getter;
import lombok.ToString;
import java.util.Map;

@ToString
@Getter
public class Badge {

    private final String badgeKey;
    private final String imageUrl;

    //    image_url_1x : "https://static-cdn.jtvnw.net/badges/v1/ca3db7f7-18f5-487e-a329-cd0b538ee979/1",
    //    image_url_2x : "https://static-cdn.jtvnw.net/badges/v1/ca3db7f7-18f5-487e-a329-cd0b538ee979/2",
    //    image_url_4x : "https://static-cdn.jtvnw.net/badges/v1/ca3db7f7-18f5-487e-a329-cd0b538ee979/3",
    //    description : "Anonymous Cheerer",
    //    title : "Anonymous Cheerer",
    //    click_action : "none",
    //    click_url : " ",
    //    last_updated : "null

    public Badge(final String badgeKey, final Map<String, String> versions) {
        this.badgeKey = badgeKey;
        this.imageUrl = versions.get("image_url_1x");
    }

}
