package de.arindy.rainbowclient.chat;

import de.arindy.rainbowclient.auth.implicit.AuthenticationError;
import de.arindy.rainbowclient.auth.implicit.Authenticator;
import de.arindy.rainbowclient.auth.implicit.Credential;
import de.arindy.rainbowclient.chat.events.ChatEvent;
import de.arindy.rainbowclient.chat.events.ConnectedEvent;
import de.arindy.rainbowclient.chat.events.DisconnectedEvent;
import de.arindy.rainbowclient.chat.events.EventListener;
import de.arindy.rainbowclient.chat.events.ReconnectEvent;
import de.arindy.rainbowclient.grants.Scopes;
import lombok.Getter;
import lombok.extern.slf4j.Slf4j;
import org.java_websocket.enums.ReadyState;
import java.util.Arrays;
import java.util.Collection;
import java.util.Timer;
import java.util.TimerTask;
import java.util.UUID;
import static de.arindy.rainbowclient.chat.ConnectionState.*;

@Slf4j
public class ChatConnecter extends EventListener {

    public static final long TIMER_PERIOD = 3600000L;
    private static final long FORCE_CLOSE_DELAY = 250L;

    @Getter
    private Credential credential;
    private String webSocketServer;
    private Authenticator authenticator;
    private Collection<EventListener> eventListeners;
    private ChatWebSocket webSocket;
    private ConnectionState connectionState = ConnectionState.DISCONNECTED;
    private Timer tokenValidator;
    private boolean forceClose;

    public ChatConnecter(final String webSocketServer, final String clientId, final Collection<EventListener> eventListeners) {
        this.webSocketServer = webSocketServer;
        this.authenticator = Authenticator.builder().clientId(clientId).scopes(Arrays.asList(Scopes.CHAT_READ, Scopes.CHAT_EDIT)).build();
        this.eventListeners = eventListeners;
    }

    public ChatConnecter authenticate(final String userName) throws AuthenticationError {
        credential = authenticator.authorize(userName.toLowerCase());
        return this;
    }

    void register(final EventListener eventListener) {
        webSocket.registerListener(eventListener);
    }

    void register(final Collection<EventListener> eventListeners) {
        webSocket.registerListener(eventListeners);
    }

    void unRegister(final EventListener eventListener) {
        webSocket.unRegisterListener(eventListener);
    }

    void unRegister(final Collection<EventListener> eventListeners) {
        webSocket.unRegisterListener(eventListeners);
    }

    public void onEvent(final ChatEvent event) {
        if (event instanceof ConnectedEvent) {
            connectionState = CONNECTED;
        } else if (event instanceof DisconnectedEvent) {
            if (!forceClose && !ConnectionState.DISCONNECTING.equals(connectionState)) {
                log.info(
                    String.format(
                        "Connection to TwitchIRC lost! [Code: %s; Reason: %s]",
                        ((DisconnectedEvent) event).code(),
                        ((DisconnectedEvent) event).reason()
                    )
                );
                reconnect();
            } else {
                connectionState = DISCONNECTED;
            }
        } else if (event instanceof ReconnectEvent) {
            reconnect();
        }
    }

    public ChatConnecter connect() {
        if (DISCONNECTED.equals(connectionState) || RECONNECTING.equals(connectionState)) {
            log.info("Connecting to TwitchIRC...");
            connectionState = ConnectionState.CONNECTING;
            webSocket = new ChatWebSocket(webSocketServer, credential).registerListener(eventListeners).registerListener(this);
            new Thread(webSocket, String.format("ChatWebSocket:%s", UUID.randomUUID())).start();
            tokenValidator = new Timer("TokenValidator");
            tokenValidator.scheduleAtFixedRate(new TokenValidator(), 0L, TIMER_PERIOD);
            connectionState = CONNECTED;
        }
        return this;
    }

    private void reconnect() {
        log.info("Reconnecting to TwitchIRC ...");
        connectionState = RECONNECTING;
        disconnect();
        connect();
    }

    void disconnect() {
        if (CONNECTED.equals(connectionState) && webSocketOpen()) {
            connectionState = DISCONNECTING;
            log.info("Disconnection from TwitchIRC ...");
            tokenValidator.cancel();
            webSocket.send("QUIT");
        }
    }

    void close() throws InterruptedException {
        forceClose = true;
        while (ReadyState.NOT_YET_CONNECTED.equals(webSocket.getReadyState())) {
            Thread.sleep(FORCE_CLOSE_DELAY);
        }
        disconnect();
    }

    private boolean webSocketOpen() {
        return webSocket != null && webSocket.isOpen();
    }

    public void send(final String command) throws NotConnectedException {
        webSocket.sendText(command);
    }

    public void send(final String command, final Object... args) throws NotConnectedException {
        webSocket.sendText(String.format(command, args));
    }

    public boolean connected() {
        return webSocket.isOpen();
    }

    private class TokenValidator extends TimerTask {

        @Override
        public void run() {
            try {
                if (!Authenticator.validate(credential)) {
                    authenticate(credential.userName());
                    reconnect();
                }
            } catch (AuthenticationError authenticationError) {
                log.error(authenticationError.getMessage(), authenticationError);
                try {
                    close();
                } catch (InterruptedException e) {
                    log.error(e.getMessage(), e);
                    Thread.currentThread().interrupt();
                }
            }
        }

    }

}
