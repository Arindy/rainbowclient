package de.arindy.rainbowclient.chat.events;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.ToString;
import java.util.Map;

@Getter
@ToString
@AllArgsConstructor
public class ClearChatEvent implements ChatEvent {

    public enum TAG {

        /**
         * (Optional) Duration of the timeout, in seconds. If omitted, the ban is permanent.
         */
        BAN_DURATION("ban-duration");

        private String key;

        TAG(final String key) {
            this.key = key;
        }
    }

    private final Map<String, String> tags;
    private final String channel;
    private final String payload;

    public boolean hasTag(final TAG tag) {
        return tags.containsKey(tag.key);
    }

    public String tag(final TAG tag) {
        return tags.get(tag.key);
    }

}
