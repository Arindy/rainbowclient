package de.arindy.rainbowclient.chat.events;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.ToString;
import java.util.Map;

@Getter
@ToString
@AllArgsConstructor
public class NoticeEvent implements ChatEvent {

    // TODO: 14/04/2020 https://dev.twitch.tv/docs/irc/msg-id
    private final Map<String, String> tags;
    private final String channel;
    private final String message;

}
