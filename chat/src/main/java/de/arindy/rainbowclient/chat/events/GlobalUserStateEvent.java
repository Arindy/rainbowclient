package de.arindy.rainbowclient.chat.events;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.ToString;
import java.util.Map;

@Getter
@ToString
@AllArgsConstructor
public class GlobalUserStateEvent implements ChatEvent {

    public enum TAG {
        /**
         * Metadata related to the chat badges in the badges tag.
         * <p>
         * Currently this is used only for subscriber, to indicate the exact number of months the user has been a subscriber.
         * This number is finer grained than the version number in badges.
         * For example, a user who has been a subscriber for 45 months would have a badge-info value of 45 but might have a badges version number for only 3 years.
         */
        BADGE_INFO("badge-info"),
        /**
         * Comma-separated list of chat badges and the version of each badge (each in the format <badge>/<version>, such as admin/1).
         * There are many valid badge values;
         * e.g., admin, bits, broadcaster, global_mod, moderator, subscriber, staff, turbo.
         * Many badges have only 1 version, but some badges have different versions (images), depending on how long you hold the badge status; e.g., subscriber.
         */
        BADGES("badges"),
        /**
         * Hexadecimal RGB color code; the empty string if it is never set.
         */
        COLOR("color"),
        /**
         * The user’s display name, escaped as described in the
         * <a href="https://ircv3.net/specs/core/message-tags-3.2.html">IRCv3 spec</a>.
         * This is empty if it is never set.
         */
        DISPLAY_NAME("display-name"),
        /**
         * A comma-separated list of emotes, belonging to one or more emote sets. This always contains at least 0.
         * <a href="https://dev.twitch.tv/docs/v5/reference/chat/#get-chat-emoticons-by-set">Get Chat Emoticons by Set</a> gets a subset of emoticons.
         */
        EMOTE_SETS("emote-sets"),
        /**
         * The user’s ID.
         */
        USER_ID("user-id");

        private String key;

        TAG(final String key) {
            this.key = key;
        }
    }

    private Map<String, String> tags;

    public boolean hasTag(final TAG tag) {
        return tags.containsKey(tag.key);
    }

    public String tag(final TAG tag) {
        return tags.get(tag.key);
    }

}
