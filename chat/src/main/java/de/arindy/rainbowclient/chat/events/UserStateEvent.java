package de.arindy.rainbowclient.chat.events;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.ToString;
import java.util.Map;

@Getter
@ToString
@AllArgsConstructor
public class UserStateEvent implements ChatEvent {

    public enum TAG {
        /**
         * Metadata related to the chat badges in the badges tag.
         * <p/>
         * Currently this is used only for subscriber, to indicate the exact number of months the user has been a subscriber.
         * This number is finer grained than the version number in badges.
         * For example, a user who has been a subscriber for 45 months would have a badge-info value of 45 but might have a badges version number for only 3 years.
         */
        BADGE_INFO("badge-info"),
        /**
         * Hexadecimal RGB color code; the empty string if it is never set.
         */
        COLOR("color"),
        /**
         * The user’s display name, escaped as described in the
         * <a href="https://ircv3.net/specs/core/message-tags-3.2.html">IRCv3 spec</a>.
         * This is empty if it is never set.
         */
        DISPLAY_NAME("display-name");

        private String key;

        TAG(final String key) {
            this.key = key;
        }

    }

    private Map<String, String> tags;
    private String channel;

    public boolean hasTag(final TAG tag) {
        return tags.containsKey(tag.key);
    }

    public String tag(final TAG tag) {
        return tags.get(tag.key);
    }

}
