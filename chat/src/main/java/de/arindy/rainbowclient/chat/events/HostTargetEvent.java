package de.arindy.rainbowclient.chat.events;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.ToString;

@Getter
@ToString
@AllArgsConstructor
public class HostTargetEvent implements ChatEvent {

    private final String hostingChannel;
    private final String payload;

}
