package de.arindy.rainbowclient.chat;

public class NotConnectedException extends Exception {

    public NotConnectedException(final String message) {
        super(message);
    }

    public NotConnectedException(final String message, final Throwable cause) {
        super(message, cause);
    }

}
