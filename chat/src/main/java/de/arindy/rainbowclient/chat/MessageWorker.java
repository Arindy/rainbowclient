package de.arindy.rainbowclient.chat;

import io.github.bucket4j.Bandwidth;
import io.github.bucket4j.Bucket;
import io.github.bucket4j.Bucket4j;
import io.github.bucket4j.local.LocalBucket;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.collections4.queue.CircularFifoQueue;
import java.time.Duration;

@Slf4j
public class MessageWorker implements Runnable {

    private final CircularFifoQueue<String> ircCommandQueue;
    private MessageSender messageSender;
    private Bucket rateLimiter;
    private boolean running;

    private static LocalBucket createRateLimiter(final int capacity, final int seconds) {
        return Bucket4j.builder().addLimit(Bandwidth.simple(capacity, Duration.ofSeconds(seconds))).build();
    }

    public MessageWorker(final int queueSize, final int capacity, final int seconds, final MessageSender messageSender) {
        ircCommandQueue = new CircularFifoQueue<>(queueSize);
        this.messageSender = messageSender;
        rateLimiter = createRateLimiter(capacity, seconds);
    }

    public MessageWorker changeRate(final int capacity, final int seconds) {
        rateLimiter = createRateLimiter(capacity, seconds);
        return this;
    }

    public void send(final String message, final String... args) throws NotConnectedException {
        send(String.format(message, args));
    }

    public void send(final String message) throws NotConnectedException {
        if (running) {
            ircCommandQueue.add(message);
        } else {
            throw new NotConnectedException("Worker not running!");
        }
    }

    public void sendWithoutQueue(final String message) throws NotConnectedException {
        messageSender.sendMessage(message);
    }

    public void stop() {
        running = false;
    }

    @Override
    public void run() {
        running = true;
        while (running) {
            try {
                if (ircCommandQueue.size() > 0) {
                    rateLimiter.asScheduler().consume(1);
                    final String message = ircCommandQueue.remove();
                    messageSender.sendMessage(message);
                    log.trace(String.format("%s messages left before hitting the rate-limit!", rateLimiter.getAvailableTokens()));
                }
                Thread.sleep(250L);
            } catch (InterruptedException | NotConnectedException e) {
                log.error(e.getMessage(), e);
            }
        }
    }

}
