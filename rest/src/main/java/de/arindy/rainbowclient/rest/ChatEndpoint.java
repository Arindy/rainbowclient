package de.arindy.rainbowclient.rest;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.glassfish.jersey.client.ClientConfig;
import javax.ws.rs.client.ClientBuilder;
import java.util.HashMap;
import java.util.Map;

public final class ChatEndpoint {

    private ChatEndpoint() {
    }

    public static Map<String, Map<String, Map<String, Map<String, String>>>> getBadges(final String channelId) {
        final Map<String, Map<String, Map<String, Map<String, String>>>> result = new HashMap<>();
        try {
            result.putAll(getBadgeSets("https://badges.twitch.tv/v1/badges/global/display"));
            result.putAll(getBadgeSets(String.format("https://badges.twitch.tv/v1/badges/channels/%s/display", channelId)));
        } catch (JsonProcessingException e) {
            e.printStackTrace();
        }
        return result;
    }

    private static Map<String, Map<String, Map<String, Map<String, String>>>> getBadgeSets(final String format1) throws JsonProcessingException {
        return new ObjectMapper().readValue(ClientBuilder.newClient(new ClientConfig())
            .target(format1)
            .request().get().readEntity(String.class), new TypeReference<Map<String, Map<String, Map<String, Map<String, Map<String, String>>>>>>() {}).get("badge_sets");
    }

}
