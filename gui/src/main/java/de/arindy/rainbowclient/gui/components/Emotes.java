package de.arindy.rainbowclient.gui.components;

import javafx.scene.image.Image;
import java.util.HashMap;
import java.util.Map;

public final class Emotes {

    private static final String EMOTES_URL = "http://static-cdn.jtvnw.net/emoticons/v1/%s/1.0";
    private static final Map<String, Image> EMOTES = new HashMap<>();

    public static Image get(final String id) {
        if (!EMOTES.containsKey(id)) {
            EMOTES.put(id, new Image(String.format(EMOTES_URL, id)));
        }
        return EMOTES.get(id);
    }

    private Emotes() {
    }

}
