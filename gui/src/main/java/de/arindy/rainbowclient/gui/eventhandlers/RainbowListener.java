package de.arindy.rainbowclient.gui.eventhandlers;

import de.arindy.rainbowclient.chat.events.ChatEvent;
import de.arindy.rainbowclient.chat.events.DisconnectedEvent;
import de.arindy.rainbowclient.chat.events.EventListener;
import de.arindy.rainbowclient.chat.events.GlobalUserStateEvent;
import de.arindy.rainbowclient.chat.events.JoinEvent;
import de.arindy.rainbowclient.chat.events.MessageEvent;
import de.arindy.rainbowclient.chat.events.PartEvent;
import de.arindy.rainbowclient.chat.events.RoomStateEvent;
import de.arindy.rainbowclient.chat.events.UserStateEvent;
import de.arindy.rainbowclient.chat.events.WelcomeEvent;
import javafx.application.Platform;

public final class RainbowListener extends EventListener {

    private final GuiUpdater guiUpdater;
    private final WelcomeEventHandler welcomeEventHandler;
    private final MessageEventHandler messageEventHandler;
    private final JoinEventHandler joinEventHandler;

    private final PartEventHandler partEventHandler;

    private final GlobalUserStateEventHandler globalUserStateEventHandler;
    private final UserStateEventHandler userStateEventHandler;
    private final DisconnectedEventHandler disconnectedEventHandler;
    private final RoomStateEventHandler roomStateEventHandler;

    public RainbowListener(final GuiUpdater guiUpdater) {
        this.guiUpdater = guiUpdater;
        welcomeEventHandler = new WelcomeEventHandler(guiUpdater);
        messageEventHandler = new MessageEventHandler(guiUpdater);
        joinEventHandler = new JoinEventHandler(guiUpdater);
        partEventHandler = new PartEventHandler(guiUpdater);
        userStateEventHandler = new UserStateEventHandler(guiUpdater);
        globalUserStateEventHandler = new GlobalUserStateEventHandler(guiUpdater);
        disconnectedEventHandler = new DisconnectedEventHandler(guiUpdater);
        roomStateEventHandler = new RoomStateEventHandler(guiUpdater);
    }

    @Override
    public void onEvent(final ChatEvent chatEvent) {
        Platform.runLater(() -> {
                guiUpdater.debug(chatEvent.toString());
                if (chatEvent instanceof WelcomeEvent) {
                    welcomeEventHandler.handle((WelcomeEvent) chatEvent);
                } else if (chatEvent instanceof MessageEvent) {
                    messageEventHandler.handle((MessageEvent) chatEvent);
                } else if (chatEvent instanceof JoinEvent) {
                    joinEventHandler.handle((JoinEvent) chatEvent);
                } else if (chatEvent instanceof PartEvent) {
                    partEventHandler.handle((PartEvent) chatEvent);
                } else if (chatEvent instanceof GlobalUserStateEvent) {
                    globalUserStateEventHandler.handle((GlobalUserStateEvent) chatEvent);
                } else if (chatEvent instanceof UserStateEvent) {
                    userStateEventHandler.handle((UserStateEvent) chatEvent);
                } else if (chatEvent instanceof DisconnectedEvent) {
                    disconnectedEventHandler.handle((DisconnectedEvent) chatEvent);
                } else if (chatEvent instanceof RoomStateEvent) {
                    roomStateEventHandler.handle((RoomStateEvent) chatEvent);
                }
            }
        );
    }

}
