package de.arindy.rainbowclient.gui;

import com.google.common.base.Strings;
import com.goxr3plus.fxborderlessscene.borderless.BorderlessScene;
import com.sun.jna.Pointer;
import com.sun.jna.platform.win32.User32;
import com.sun.jna.platform.win32.WinDef;
import de.arindy.rainbowclient.auth.implicit.AuthenticationError;
import de.arindy.rainbowclient.chat.Channel;
import de.arindy.rainbowclient.chat.Chat;
import de.arindy.rainbowclient.chat.Color;
import de.arindy.rainbowclient.chat.NotConnectedException;
import de.arindy.rainbowclient.gui.components.ChatMessage;
import de.arindy.rainbowclient.gui.components.ChatMessages;
import de.arindy.rainbowclient.gui.components.Counters;
import de.arindy.rainbowclient.gui.components.WrappingListCell;
import de.arindy.rainbowclient.gui.eventhandlers.GuiUpdater;
import de.arindy.rainbowclient.gui.eventhandlers.RainbowListener;
import javafx.application.Application;
import javafx.application.Platform;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.ListView;
import javafx.scene.control.SplitPane;
import javafx.scene.control.TextField;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;
import javafx.stage.StageStyle;
import jfxtras.styles.jmetro.JMetro;
import jfxtras.styles.jmetro.Style;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.collections4.Closure;
import org.controlsfx.control.ToggleSwitch;
import java.io.IOException;
import java.net.URL;
import java.util.EnumSet;
import java.util.Map;
import java.util.ResourceBundle;
import static com.sun.jna.platform.win32.WinUser.GWL_STYLE;
import static com.sun.jna.platform.win32.WinUser.WS_MINIMIZEBOX;

@Slf4j
public class RainbowClient extends Application implements Initializable, GuiUpdater {

    public static final String TITLE = "RainbowClient";
    private static final int MIN_WIDTH = 600;
    private static final int MIN_HEIGHT = 800;
    private static boolean debugEnabled;

    private final EnumSet<Color> colors = EnumSet.allOf(Color.class);
    private final ObservableList<String> debugMessages = FXCollections.observableArrayList();
    private final ListView<String> debug = new ListView<>();

    @FXML
    private SplitPane messagePane;
    @FXML
    private ChatMessages messages;
    @FXML
    private TextField messageInput;
    @FXML
    private TextField channel;
    @FXML
    private ToggleSwitch colorCycle;
    @FXML
    private Button sendButton;
    @FXML
    private Label channelLabel;
    @FXML
    private Button maximizeButton;
    @FXML
    private TextField userName;
    @FXML
    private Button joinChannelBtn;
    @FXML
    private Button leaveChannelBtn;
    @FXML
    private Button connectBtn;
    @FXML
    public VBox counterPane;

    private Chat chat;
    private Channel joinedChannel;
    private int colorIndex;
    private String initialColor;
    private BorderlessScene borderlessScene;
    private static EventHandler<ActionEvent> maximizeStage;
    private static EventHandler<ActionEvent> minimizeStage;
    private static Closure<String> setTitle;
    private Configuration configuration;
    private Counters counters;

    public static void main(final String[] args) {
        for (String arg : args) {
            if ("DEBUG".equals(arg)) {
                debugEnabled = true;
                break;
            }
        }
        launch(args);
    }

    @Override
    public void start(final Stage stage) throws Exception {
        final FXMLLoader loader = new FXMLLoader(getClass().getResource("/fxml/RainbowClient.fxml"));
        final Scene scene = new Scene(loader.load());
        this.borderlessScene = new BorderlessScene(stage, StageStyle.UNDECORATED, scene.getRoot(), MIN_WIDTH, MIN_HEIGHT);
        borderlessScene.removeDefaultCSS();
        borderlessScene.setMoveControl(scene.getRoot());

        borderlessScene.maximizedProperty().addListener(observable -> loader.<RainbowClient>getController().changeMaximizeButtonText(borderlessScene.isMaximized() ? "\uE923" : "\uE922"));
        maximizeStage = event -> borderlessScene.maximizeStage();
        minimizeStage = event -> borderlessScene.minimizeStage();

        setTitle = stage::setTitle;

        stage.setScene(borderlessScene);
        new JMetro(Style.DARK).setScene(borderlessScene);

        stage.setTitle(TITLE);
        stage.show();
        final WinDef.HWND hwnd = new WinDef.HWND(new Pointer(com.sun.glass.ui.Window.getWindows().get(0).getNativeWindow()));
        final int minimized = User32.INSTANCE.GetWindowLong(hwnd, GWL_STYLE) | WS_MINIMIZEBOX;
        User32.INSTANCE.SetWindowLong(hwnd, GWL_STYLE, minimized);
    }

    private void changeMaximizeButtonText(final String text) {
        maximizeButton.setText(text);
    }

    @Override
    public void initialize(final URL location, final ResourceBundle resources) {
        try {
            configuration = Configuration.load(getClass().getResourceAsStream("/config.yaml"));
            initializeCounter();
            log.debug(String.format("Configuration loaded: %s", configuration));
        } catch (IOException e) {
            log.error(e.getMessage(), e);
        }
        enableDebug();
        debug.setItems(debugMessages);
        debug.setCellFactory(WrappingListCell::new);
        colorCycle.selectedProperty().addListener(observable -> {
            try {
                colorCycleAction();
            } catch (NotConnectedException e) {
                log.error(e.getMessage(), e);
            }
        });
        messageInput.textProperty().addListener(observable -> sendButton.setDisable(Strings.isNullOrEmpty(messageInput.getText())));
    }

    private void enableDebug() {
        if (debugEnabled) {
            messagePane.getItems().add(debug);
        }
    }

    public void onUserNameEnter(final KeyEvent event) {
        if (KeyCode.ENTER.equals(event.getCode())) {
            connectToChat();
        }
    }

    public void connectToChat() {
        Platform.runLater(() -> {
            try {
                chat = Chat.builder().webSocketServer(configuration.api().webSocketServer()).clientId(configuration.client().id()).userName(userName.getText())
                    .eventListener(new RainbowListener(this)).connect();
            } catch (AuthenticationError error) {
                log.error(error.getMessage(), error);
            }
        });
        disconnected(false);
    }

    public void onChannelEnter(final KeyEvent event) throws NotConnectedException {
        if (KeyCode.ENTER.equals(event.getCode())) {
            joinChannel();
        }
    }

    public void joinChannel() throws NotConnectedException {
        joinedChannel = chat.joinChannel(channel.getText());
    }

    public void onMessageEnter(final KeyEvent event) throws NotConnectedException {
        if (KeyCode.ENTER.equals(event.getCode())) {
            onSend();
        }
    }

    public void onSend() throws NotConnectedException {
        sendMessage(messageInput.getText());
    }

    public void sendMessage(final String message) throws NotConnectedException {
        if (!message.isEmpty()) {
            messageInput.setText(message);
            if (colorCycle.isSelected()) {
                final Color color = nextColor();
                chat.changeColor(color.toString());
                changeNameColor(color.toString());
            }
            joinedChannel.sendMessage(message);
            messageInput.setDisable(true);
        }
    }

    private Color nextColor() {
        if (colorIndex < colors.size() - 1) {
            colorIndex++;
        } else {
            colorIndex = 0;
        }
        return colors.toArray(new Color[0])[colorIndex];
    }

    public void leaveChannel() throws NotConnectedException {
        chat.leaveChannel(joinedChannel);
    }

    public void maximize(final ActionEvent event) {
        maximizeStage.handle(event);
    }

    public void minimize(final ActionEvent actionEvent) {
        minimizeStage.handle(actionEvent);
    }

    public void colorCycleAction() throws NotConnectedException {
        if (!colorCycle.isSelected() && chat != null) {
            chat.changeColor(initialColor);
        }
    }

    public void close() throws Exception {
        if (chat != null && chat.connected()) {
            chat.changeColor(initialColor);
            chat.disconnect();
        }
        Platform.exit();
    }

    @Override
    public String userName() {
        return userName.getText();
    }

    @Override
    public String messageInput() {
        return messageInput.getText();
    }

    @Override
    public void disconnected(final boolean disconnected) {
        connectBtn.setDisable(!disconnected);
        userName.setDisable(!disconnected);
        if (disconnected) {
            joinChannelBtn.setDisable(true);
        }
    }

    @Override
    public void addMessage(final ChatMessage message) {
        messages.add(joinedChannel, message);
    }

    @Override
    public void debug(final String message) {
        if (debugEnabled) {
            debugMessages.add(message);
            debug.scrollTo(debugMessages.size() - 1);
        }
    }

    @Override
    public void clearMessages() {
        clearMessageInput();
        messages.clear();
    }

    @Override
    public void clearMessageInput() {
        messageInput.clear();
    }

    @Override
    public void disableMessageInput() {
        messageInput.setDisable(true);
    }

    public void enableMessageInput() {
        messageInput.setDisable(false);
        messageInput.requestFocus();
    }

    @Override
    public void changeChannelLabel(final String channel) {
        channelLabel.setText(channel);
    }

    @Override
    public void changeTitle(final String newTitle) {
        setTitle.execute(newTitle);
    }

    @Override
    public void changeChannelJoined(final boolean joined) {
        joinChannelBtn.setDisable(joined);
        leaveChannelBtn.setDisable(!joined);
    }

    @Override
    public void setInitialColor(final String color) {
        initialColor = color;
    }

    @Override
    public void changeNameColor(final String color) {
        if (!Strings.isNullOrEmpty(color)) {
            userName.setStyle("-fx-text-fill: " + color);
        }
    }

    @Override
    public void enableJoinChannelButton(final boolean enable) {
        joinChannelBtn.setDisable(!enable);
    }

    @Override
    public void changeUserName(final String displayName, final String color) {
        if (displayName != null) {
            userName.setText(displayName);
        }
        changeNameColor(color);
    }

    @Override
    public void updateChannel(final Map<String, String> tags) {
        joinedChannel.updateTags(tags);
    }

    private void initializeCounter() {
        this.counters = Counters.load(this::sendMessage);
        for (Counters.Counter counter : counters.counter()) {
            addCounter(counter);
        }
    }

    public void addCounter() {
        addCounter(counters.addCounter("", 0));
    }

    public void addCounter(final Counters.Counter counter) {
        counterPane.getChildren().add(counter);
    }

}
