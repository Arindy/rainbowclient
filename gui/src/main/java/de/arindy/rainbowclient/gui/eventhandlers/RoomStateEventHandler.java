package de.arindy.rainbowclient.gui.eventhandlers;

import de.arindy.rainbowclient.chat.events.RoomStateEvent;

public class RoomStateEventHandler {

    private final GuiUpdater client;

    public RoomStateEventHandler(final GuiUpdater client) {
        this.client = client;
    }

    public void handle(final RoomStateEvent event) {
        client.updateChannel(event.tags());
    }

}
