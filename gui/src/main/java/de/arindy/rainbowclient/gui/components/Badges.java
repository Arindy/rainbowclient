package de.arindy.rainbowclient.gui.components;

import javafx.scene.image.Image;
import java.util.HashMap;
import java.util.Map;

public final class Badges {

    private static final Map<String, Image> EMOTES = new HashMap<>();

    public static Image get(final String url) {
        if (!EMOTES.containsKey(url)) {
            EMOTES.put(url, new Image(url));
        }
        return EMOTES.get(url);
    }

    private Badges() {
    }

}
