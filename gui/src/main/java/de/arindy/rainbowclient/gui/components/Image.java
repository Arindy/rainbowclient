package de.arindy.rainbowclient.gui.components;

import javafx.scene.Node;

public interface Image {

    Node createNode();

    int getLength();

}
