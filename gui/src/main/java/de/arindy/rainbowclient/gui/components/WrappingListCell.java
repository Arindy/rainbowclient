package de.arindy.rainbowclient.gui.components;

import javafx.beans.value.ObservableValue;
import javafx.scene.control.ListCell;
import javafx.scene.control.ListView;

public class WrappingListCell extends ListCell<String> {


    public WrappingListCell(final ListView<String> param) {
        param.widthProperty().addListener(this::updateCellWidth);
    }

    private void updateCellWidth(final ObservableValue<? extends Number> observable, final Number oldValue, final Number newValue) {
        final double width = newValue.doubleValue() - 15;
        setMinWidth(width);
        setMaxWidth(width);
        setPrefWidth(width);
    }

    @Override
    protected void updateItem(final String item, final boolean empty) {
        super.updateItem(item, empty);
        if (empty || item == null) {
            setGraphic(null);
            setText(null);
        } else {
            setWrapText(true);
            setText(item);
        }
    }

}
