package de.arindy.rainbowclient.gui.components;

import javafx.scene.Node;
import javafx.scene.image.ImageView;
import lombok.Getter;

@Getter
public class Badge implements Image {

    private final String id;
    private final String imageUrl;

    public Badge(final String id, final String imageUrl) {
        this.id = id;
        this.imageUrl = imageUrl;
    }

    @Override
    public Node createNode() {
        return new ImageView(Badges.get(imageUrl));
    }

    @Override
    public int getLength() {
        return id.length();
    }

    @Override
    public String toString() {
        return id;
    }

}
