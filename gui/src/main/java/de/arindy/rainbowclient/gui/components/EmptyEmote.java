package de.arindy.rainbowclient.gui.components;

import javafx.scene.Node;

public class EmptyEmote implements Image {

    @Override
    public Node createNode() {
        throw new AssertionError("Unreachable code");
    }

    @Override
    public int getLength() {
        return 0;
    }

}
