package de.arindy.rainbowclient.gui.eventhandlers;

import de.arindy.rainbowclient.chat.events.WelcomeEvent;
import de.arindy.rainbowclient.gui.components.ChatMessage;

public class WelcomeEventHandler {

    private final GuiUpdater client;

    public WelcomeEventHandler(final GuiUpdater client) {
        this.client = client;
    }

    public void handle(final WelcomeEvent event) {
        client.addMessage(ChatMessage.builder().text(event.message()).build());
    }

}
