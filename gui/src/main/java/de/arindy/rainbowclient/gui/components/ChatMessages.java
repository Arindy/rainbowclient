package de.arindy.rainbowclient.gui.components;

import de.arindy.rainbowclient.chat.Badge;
import de.arindy.rainbowclient.chat.Channel;
import de.arindy.rainbowclient.chat.Color;
import org.fxmisc.richtext.GenericStyledArea;
import org.fxmisc.richtext.StyledTextArea;
import org.fxmisc.richtext.model.SegmentOps;
import org.reactfx.util.Either;
import java.util.Optional;

public class ChatMessages extends GenericStyledArea<ChatStyle, Either<String, Image>, MessageStyle> {

    public ChatMessages() {
        super(new ChatStyle(),
            (paragraph, style) -> paragraph.setStyle(style.toCss()),
            MessageStyle.EMPTY,
            SegmentOps.<MessageStyle>styledTextOps()._or(new EmoteImageOps<>(), (o, o2) -> Optional.empty()),
            seg -> seg.getSegment().unify(text -> StyledTextArea.createStyledTextNode(text, seg.getStyle(), (textToStyle, style) -> textToStyle.setStyle(style.toCss())), Image::createNode));
        setEditable(false);
        setWrapText(true);
    }

    public void add(final Channel joinedChannel, final ChatMessage message) {
        appendNewLine();
        appendBadges(joinedChannel, message);
        appendUser(message);
        appendMessage(message);
        end(SelectionPolicy.CLEAR);
        requestFollowCaret();
    }

    private void appendBadges(final Channel joinedChannel, final ChatMessage message) {
        message.badges().forEach((name, version) -> {
            final Badge badge = joinedChannel.badge(name, version);
            append(Either.right(new de.arindy.rainbowclient.gui.components.Badge(badge.badgeKey(), badge.imageUrl())), MessageStyle.EMPTY);
        });
        append(Either.left(" "), MessageStyle.EMPTY);
    }

    private void appendNewLine() {
        if (getContent().length() > 0) {
            append(Either.left("\n\n"), MessageStyle.builder().fontSize(2).build());
        }
    }

    private void appendUser(final ChatMessage message) {
        if (message.user() != null) {
            final String user;
            if (message.displayName() == null) {
                user = message.user();
            } else {
                user = message.displayName();
            }
            append(Either.left(user), MessageStyle.builder().textColor(color(message.color())).bold(true).build());
        }
    }

    private String color(final String color) {
        return color != null ? color : Color.GOLDEN_ROD.toString();
    }

    private void appendMessage(final ChatMessage message) {
        final MessageStyle messageStyle;
        if (message.meMessage()) {
            append(Either.left(" "), MessageStyle.EMPTY);
            messageStyle = MessageStyle.builder().textColor(color(message.color())).build();
        } else {
            append(Either.left(": "), MessageStyle.EMPTY);
            messageStyle = MessageStyle.EMPTY;
        }
        for (Either<String, Image> segment : message.segments()) {
            append(segment, messageStyle);
        }
    }

}
