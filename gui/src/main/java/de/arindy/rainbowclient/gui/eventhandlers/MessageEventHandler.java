package de.arindy.rainbowclient.gui.eventhandlers;

import de.arindy.rainbowclient.chat.events.MessageEvent;
import de.arindy.rainbowclient.gui.components.ChatMessage;

public class MessageEventHandler {

    private final GuiUpdater client;

    public MessageEventHandler(final GuiUpdater client) {
        this.client = client;
    }

    public void handle(final MessageEvent event) {
        client.addMessage(ChatMessage.builder().user(event.user()).tags(event.tags()).text(event.message()).build());
    }

}
