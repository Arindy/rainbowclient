package de.arindy.rainbowclient.gui.components;

import lombok.Getter;
import lombok.ToString;
import lombok.extern.slf4j.Slf4j;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.Objects;

@Getter
@Slf4j
@ToString
public final class EmoteReplacement implements Comparable<EmoteReplacement> {

    private final String id;
    private final Range range;

    public static List<EmoteReplacement> parse(final String emotes) {
        final List<EmoteReplacement> result;
        if (emotes == null) {
            result = Collections.emptyList();
        } else {
            result = emoteReplacements(emotes);
        }
        Collections.sort(result);
        return result;
    }

    private static List<EmoteReplacement> emoteReplacements(final String emotes) {
        final List<EmoteReplacement> result = new ArrayList<>();
        for (String emote : emotes.split("/")) {
            final String[] emoteReplacement = emote.split(":");
            if (emoteReplacement.length == 2) {
                result.addAll(emoteReplacements(emoteReplacement[0], emoteReplacement[1].split(",")));
            }
        }
        return result;
    }

    private static List<EmoteReplacement> emoteReplacements(final String id, final String[] ranges) {
        final List<EmoteReplacement> result = new ArrayList<>();
        for (String range : ranges) {
            final String[] rangeSplit = range.split("-");
            if (rangeSplit.length == 2) {
                try {
                    result.add(new EmoteReplacement(id, new Range(rangeSplit[0], rangeSplit[1])));
                } catch (NumberFormatException e) {
                    log.error(e.getMessage(), e);
                }
            }
        }
        return result;
    }

    private EmoteReplacement(final String id, final Range range) {
        this.id = id;
        this.range = range;
    }

    @Override
    public boolean equals(final Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        final EmoteReplacement that = (EmoteReplacement) o;
        return id.equals(that.id) &&
            range.equals(that.range);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, range);
    }

    @Override
    public int compareTo(final EmoteReplacement o) {
        return Objects.compare(range, o.range, Comparator.naturalOrder());
    }

    @Getter
    @ToString
    public static final class Range implements Comparable<Range> {

        private final int start;
        private final int end;

        public Range(final String start, final String end) {
            this.start = Integer.parseInt(start);
            this.end = Integer.parseInt(end);
        }

        @Override
        public int compareTo(final Range o) {
            return Integer.compare(start, o.start);
        }
    }

}
