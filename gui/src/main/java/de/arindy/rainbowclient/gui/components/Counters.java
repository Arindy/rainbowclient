package de.arindy.rainbowclient.gui.components;

import com.fasterxml.jackson.annotation.JsonAutoDetect;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonSetter;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.dataformat.yaml.YAMLFactory;
import de.arindy.rainbowclient.chat.MessageSender;
import de.arindy.rainbowclient.chat.NotConnectedException;
import javafx.application.Platform;
import javafx.scene.control.Button;
import javafx.scene.control.TextField;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import lombok.Getter;
import lombok.ToString;
import lombok.extern.slf4j.Slf4j;
import java.io.File;
import java.io.IOException;
import java.util.HashSet;
import java.util.Objects;
import java.util.Set;
import java.util.UUID;
import static com.fasterxml.jackson.annotation.JsonAutoDetect.Visibility.NONE;

@ToString
@Getter
@Slf4j
public final class Counters {

    private static final File FILE = new File("counter.yaml");

    public static Counters load(final MessageSender messageSender) {
        Counters result;
        try {
            result = new ObjectMapper(new YAMLFactory()).readValue(FILE, Counters.class);
        } catch (IOException e) {
            log.warn(e.getMessage(), e);
            result = new Counters();
        }
        return result.withMessageSender(messageSender);
    }

    @JsonProperty("counter")
    @JsonDeserialize(contentUsing = NestedDeserializer.class)
    private final Set<Counter> counter = new HashSet<>();

    @JsonIgnore
    private MessageSender client;

    private Counters withMessageSender(final MessageSender client) {
        this.client = client;
        return this;
    }

    private void save() throws IOException {
        new ObjectMapper().writeValue(FILE, this);
    }

    public Counter addCounter(final String name, final int amount) {
        final Counter result = new Counter(name, amount);
        this.counter.add(result);
        return result;
    }

    public void updateCounter(final String id, final String name, final int amount) throws IOException {
        if (counterExists(id)) {
            counter(id).update(name, amount);
        } else {
            counter.add(new Counter(id, name, amount));
        }
        save();
    }

    public void removeCounter(final Counter counter) throws IOException {
        ((VBox) counter.getParent()).getChildren().remove(counter);
        this.counter.remove(counter);
        save();
    }

    private Counter counter(final String id) {
        Counter result = null;
        for (Counter count : this.counter) {
            if (Objects.equals(count.id, id)) {
                result = count;
                break;
            }
        }
        return result;
    }

    private boolean counterExists(final String id) {
        return counter(id) != null;
    }

    @ToString
    @Getter
    @JsonAutoDetect(
        fieldVisibility = NONE,
        setterVisibility = NONE,
        getterVisibility = NONE,
        isGetterVisibility = NONE,
        creatorVisibility = NONE
    )
    @JsonIgnoreProperties(ignoreUnknown = true)
    public final class Counter extends VBox {

        public static final int PLUS_ONE_WIDTH = 50;

        @JsonProperty("id")
        private String id;
        @JsonProperty("name")
        private String name;
        @JsonProperty("amount")
        private int amount;
        @JsonIgnore
        private final TextField nameField;

        @JsonIgnore
        private final TextField amountField;

        public Counter(final String name, final int amount) {
            this(UUID.randomUUID().toString(), name, amount);
        }

        public Counter(final String id, final String name, final int amount) {
            this();
            id(id);
            name(name);
            amount(amount);
        }

        public Counter() {
            this.nameField = new TextField();
            this.amountField = new TextField("0");
            final Button add = new Button("+1");
            add.setPrefWidth(PLUS_ONE_WIDTH);
            final HBox countAmount = new HBox();
            countAmount.getChildren().add(amountField);
            countAmount.getChildren().add(add);
            final Button remove = new Button("-");
            remove.getStyleClass().addAll("minMaxCloseButton", "closeButton");
            final HBox title = new HBox();
            title.getChildren().add(nameField);
            title.getChildren().add(remove);
            getChildren().add(title);
            getChildren().add(countAmount);
            remove.setOnAction(event -> {
                try {
                    removeCounter(this);
                } catch (IOException e) {
                    log.warn(e.getMessage(), e);
                }
            });
            add.setOnAction(event -> handleEvent());
        }

        @JsonSetter("id")
        public void id(final String id) {
            this.id = id;
        }

        @JsonSetter("name")
        public void name(final String name) {
            this.name = name;
            this.nameField.setText(name);
        }

        @JsonSetter("amount")
        public void amount(final int amount) {
            this.amount = amount;
            this.amountField.setText(String.valueOf(amount));
        }

        private void handleEvent() {
            try {
                name = nameField.getText();
                amount = Integer.parseInt(amountField.getText()) + 1;
                Platform.runLater(() -> amountField.setText(String.valueOf(amount)));
                updateCounter(id, name, amount);
                client.sendMessage(String.format("%s: %s", name, amount));
            } catch (NumberFormatException | NotConnectedException | IOException e) {
                log.warn(e.getMessage(), e);
            }
        }

        @Override
        public boolean equals(final Object o) {
            if (this == o) {
                return true;
            }
            if (o == null || getClass() != o.getClass()) {
                return false;
            }
            final Counter counter = (Counter) o;
            return Objects.equals(id, counter.id);
        }

        @Override
        public int hashCode() {
            return Objects.hash(id);
        }

        public void update(final String name, final Integer amount) {
            this.name = name;
            this.amount = amount;
        }

    }

}
