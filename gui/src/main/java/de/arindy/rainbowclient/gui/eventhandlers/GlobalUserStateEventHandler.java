package de.arindy.rainbowclient.gui.eventhandlers;

import de.arindy.rainbowclient.chat.events.GlobalUserStateEvent;

public class GlobalUserStateEventHandler {

    private final GuiUpdater client;

    public GlobalUserStateEventHandler(final GuiUpdater client) {
        this.client = client;
    }

    public void handle(final GlobalUserStateEvent event) {
        client.setInitialColor(event.tag(GlobalUserStateEvent.TAG.COLOR));
        client.changeNameColor(event.tag(GlobalUserStateEvent.TAG.COLOR));
        client.enableJoinChannelButton(true);
    }

}
