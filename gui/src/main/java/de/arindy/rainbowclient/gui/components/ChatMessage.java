package de.arindy.rainbowclient.gui.components;

import lombok.AccessLevel;
import lombok.Builder;
import lombok.Getter;
import org.reactfx.util.Either;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

@Getter
public class ChatMessage {

    private static final String ME_REGEX = "\\u0001ACTION (.*)\\u0001";

    private final String user;
    private final String text;
    private final Matcher matcher;
    @Getter(AccessLevel.NONE)
    private final Map<String, String> tags;

    @Builder
    public ChatMessage(final String user, final String text, final Map<String, String> tags) {
        this.user = user;
        this.text = text;
        this.matcher = Pattern.compile(ME_REGEX).matcher(text);
        if (tags == null) {
            this.tags = Collections.emptyMap();
        } else {
            this.tags = tags;
        }
    }

    public Collection<Either<String, Image>> segments() {
        final Collection<Either<String, Image>> result = new ArrayList<>();
        final String text = text();
        final List<EmoteReplacement> emoteReplacements = emoteReplacements();
        for (int i = 0; i < emoteReplacements.size(); i++) {
            final EmoteReplacement emoteReplacement = emoteReplacements.get(i);
            final EmoteReplacement.Range range = emoteReplacement.range();
            result.add(Either.left(text.substring(i == 0 ? 0 : emoteReplacements.get(i - 1).range().end() + 1, range.start())));
            result.add(Either.right(new Emote(emoteReplacement.id(), replacemet(text, range))));
        }
        final String endText;
        if (emoteReplacements.isEmpty()) {
            endText = text;
        } else {
            final int lastEmoteEnd = emoteReplacements.get(emoteReplacements.size() - 1).range().end();
            if (lastEmoteEnd == text.length()) {
                endText = "";
            } else {
                endText = text.substring(lastEmoteEnd + 1);
            }
        }
        result.add(Either.left(endText));
        return result;
    }

    private String replacemet(final String text, final EmoteReplacement.Range range) {
        final String result;
        if (text.length() == range.end()) {
            result = text.substring(range.start());
        } else {
            result = text.substring(range.start(), range.end());
        }
        return result;
    }

    public String text() {
        final String result;
        if (meMessage()) {
            result = matcher.group(1);
        } else {
            result = text;
        }
        return result;
    }

    public boolean meMessage() {
        return matcher.matches();
    }

    public String color() {
        return tags.get("color");
    }

    public String displayName() {
        return tags.get("display-name");
    }

    public Map<String, String> badges() {
        final Map<String, String> result = new HashMap<>();
        final String badges = tags.get("badges");
        if (badges != null) {
            for (String badge : badges.split(",")) {
                final String[] split = badge.split("/");
                result.put(split[0], split[1]);
            }
        }
        return result;
    }

    private List<EmoteReplacement> emoteReplacements() {
        final List<EmoteReplacement> result;
        if (tags.get("emotes") == null) {
            result = Collections.emptyList();
        } else {
            result = EmoteReplacement.parse(tags.get("emotes"));
        }
        return result;
    }

}
