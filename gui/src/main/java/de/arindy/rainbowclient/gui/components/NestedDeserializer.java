package de.arindy.rainbowclient.gui.components;

import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JsonDeserializer;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.deser.ResolvableDeserializer;
import com.fasterxml.jackson.databind.deser.std.StdDeserializer;
import java.io.IOException;

public final class NestedDeserializer extends StdDeserializer<Counters.Counter> implements ResolvableDeserializer {

    private JsonDeserializer<Object> underlyingDeserializer;

    public NestedDeserializer() {
        super(Counters.Counter.class);
    }

    @Override
    public void resolve(final DeserializationContext ctxt) throws JsonMappingException {
        underlyingDeserializer = ctxt.findRootValueDeserializer(ctxt.getTypeFactory().constructType(Counters.Counter.class));
    }

    @Override
    public Counters.Counter deserialize(final JsonParser p, final DeserializationContext ctxt)
        throws IOException {
        final Counters container = (Counters) p.getParsingContext().getParent().getParent().getCurrentValue();
        final Counters.Counter value = container.new Counter();
        underlyingDeserializer.deserialize(p, ctxt, value);
        return value;
    }

}
