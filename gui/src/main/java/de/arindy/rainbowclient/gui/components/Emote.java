package de.arindy.rainbowclient.gui.components;

import javafx.scene.Node;
import javafx.scene.image.ImageView;
import lombok.Getter;

@Getter
public class Emote implements Image {

    private final String id;
    private final String emoteString;

    public Emote(final String id, final String emoteString) {
        this.id = id;
        this.emoteString = emoteString;
    }

    @Override
    public Node createNode() {
        return new ImageView(Emotes.get(id));
    }

    @Override
    public int getLength() {
        return emoteString.length();
    }

    @Override
    public String toString() {
        return emoteString;
    }

}
