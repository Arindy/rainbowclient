package de.arindy.rainbowclient.gui.components;

import org.fxmisc.richtext.model.NodeSegmentOpsBase;

public class EmoteImageOps<S> extends NodeSegmentOpsBase<Image, S> {

    public EmoteImageOps() {
        super(new EmptyEmote());
    }

    @Override
    public int length(final Image emote) {
        return emote.getLength();
    }

}
