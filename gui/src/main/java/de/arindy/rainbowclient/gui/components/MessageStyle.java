package de.arindy.rainbowclient.gui.components;

import com.google.common.base.Strings;
import javafx.scene.paint.Color;
import lombok.Builder;

public class MessageStyle {

    public static final MessageStyle EMPTY = MessageStyle.builder().build();

    @Builder.Default
    private Color textColor = Color.WHITE;
    private boolean bold;
    @Builder.Default
    private int fontSize = 14;

    static String cssColor(final Color color) {
        final int red = (int) (color.getRed() * 255);
        final int green = (int) (color.getGreen() * 255);
        final int blue = (int) (color.getBlue() * 255);
        return "rgb(" + red + ", " + green + ", " + blue + "); mix-blend-mode: difference;";
    }

    @Builder
    public MessageStyle(final String textColor, final boolean bold, final int fontSize) {
        if (!Strings.isNullOrEmpty(textColor)) {
            this.textColor = Color.web(textColor);
        }
        this.bold = bold;
        this.fontSize = fontSize;
    }

    public String toCss() {
        final StringBuilder sb = new StringBuilder();
        if (textColor != null) {
            sb.append("-fx-fill: ").append(cssColor(textColor)).append(";");
        }
        if (bold) {
            sb.append("-fx-font-weight: bold;");
        }
        sb.append("-fx-font-size: ").append(fontSize).append("pt;");
        return sb.toString();
    }

}
