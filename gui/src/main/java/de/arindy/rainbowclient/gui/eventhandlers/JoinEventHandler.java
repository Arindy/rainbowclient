package de.arindy.rainbowclient.gui.eventhandlers;

import de.arindy.rainbowclient.chat.events.JoinEvent;
import static de.arindy.rainbowclient.gui.RainbowClient.TITLE;

public class JoinEventHandler {

    private final GuiUpdater client;

    public JoinEventHandler(final GuiUpdater client) {
        this.client = client;
    }

    public void handle(final JoinEvent event) {
        if (client.userName().equalsIgnoreCase(event.user())) {
            client.clearMessages();
            client.enableMessageInput();
            client.changeChannelLabel(event.channel());
            client.changeTitle(String.format("%s: %s", TITLE, event.channel()));
            client.changeChannelJoined(true);
        }
    }

}
