package de.arindy.rainbowclient.gui.eventhandlers;

import de.arindy.rainbowclient.chat.events.PartEvent;
import de.arindy.rainbowclient.gui.components.ChatMessage;
import static de.arindy.rainbowclient.gui.RainbowClient.TITLE;

public class PartEventHandler {

    private final GuiUpdater client;

    public PartEventHandler(final GuiUpdater client) {
        this.client = client;
    }

    public void handle(final PartEvent event) {
        if (client.userName().equalsIgnoreCase(event.user())) {
            client.clearMessageInput();
            client.addMessage(ChatMessage.builder().text(String.format("%s left channel %s", event.user(), event.channel())).build());
            client.disableMessageInput();
            client.changeChannelLabel("<no channel>");
            client.changeTitle(TITLE);
            client.changeChannelJoined(false);
        }
    }

}
