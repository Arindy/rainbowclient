package de.arindy.rainbowclient.gui.eventhandlers;

import de.arindy.rainbowclient.chat.events.DisconnectedEvent;
import de.arindy.rainbowclient.gui.components.ChatMessage;
import static de.arindy.rainbowclient.gui.RainbowClient.TITLE;

public class DisconnectedEventHandler {

    private final GuiUpdater client;

    public DisconnectedEventHandler(final GuiUpdater client) {
        this.client = client;
    }

    public void handle(final DisconnectedEvent event) {
        client.clearMessageInput();
        client.addMessage(ChatMessage.builder().text("Disconnected!").build());
        client.disableMessageInput();
        client.changeChannelLabel("<no channel>");
        client.changeTitle(TITLE);
        client.changeChannelJoined(false);
        client.disconnected(true);
    }

}
