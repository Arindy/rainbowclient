package de.arindy.rainbowclient.gui.eventhandlers;

import com.google.common.base.Strings;
import de.arindy.rainbowclient.chat.events.UserStateEvent;
import de.arindy.rainbowclient.gui.components.ChatMessage;
import static de.arindy.rainbowclient.chat.events.UserStateEvent.TAG.COLOR;
import static de.arindy.rainbowclient.chat.events.UserStateEvent.TAG.DISPLAY_NAME;

public class UserStateEventHandler {

    private final GuiUpdater client;

    public UserStateEventHandler(final GuiUpdater client) {
        this.client = client;
    }

    public void handle(final UserStateEvent event) {
        final String userName = event.tag(DISPLAY_NAME) != null ? event.tag(DISPLAY_NAME) : client.userName();
        client.changeUserName(userName, event.tag(COLOR));
        if (!Strings.isNullOrEmpty(client.messageInput())) {
            client.addMessage(ChatMessage.builder().user(userName).tags(event.tags()).text(client.messageInput()).build());
        }
        client.clearMessageInput();
        client.enableMessageInput();
    }

}
