package de.arindy.rainbowclient.gui.eventhandlers;

import de.arindy.rainbowclient.gui.components.ChatMessage;
import java.util.Map;

public interface GuiUpdater {

    String userName();

    String messageInput();

    void disconnected(boolean disconnected);

    void addMessage(ChatMessage message);

    void debug(String message);

    void clearMessages();

    void clearMessageInput();

    void disableMessageInput();

    void enableMessageInput();

    void changeChannelLabel(String channel);

    void changeTitle(String newTitle);

    void changeChannelJoined(boolean joined);

    void setInitialColor(String color);

    void changeNameColor(String tag);

    void enableJoinChannelButton(boolean enable);

    void changeUserName(String displayName, String color);

    void updateChannel(Map<String, String> tags);
}
