package de.arindy.rainbowclient.gui;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.dataformat.yaml.YAMLFactory;
import lombok.Getter;
import lombok.ToString;
import java.io.IOException;
import java.io.InputStream;

@Getter
@ToString
public class Configuration {

    public static Configuration load(final InputStream file) throws IOException {
        return new ObjectMapper(new YAMLFactory()).readValue(file, Configuration.class);
    }

    @JsonProperty("client")
    private Client client;
    @JsonProperty("api")
    private Api api;

    @Getter
    @ToString
    public static class Client {

        @JsonProperty("id")
        private String id;

    }

    @Getter
    @ToString
    public static class Api {

        @JsonProperty("webSocketServer")
        private String webSocketServer;

    }

}
